#ifndef VECTOR_HPP
# define VECTOR_HPP

#include <memory>
#include <cstddef>
#include "../includes/iterators.hpp"
#include "../includes/ft_utils.hpp"

namespace ft	{
	template < class T, class Allocator = std::allocator<T> >
	class vector	{
		public:
		/*********************************/
		/* Member types for vector class */
		/*********************************/
		typedef	T												value_type;
		typedef Allocator										allocator_type;
		typedef size_t											size_type;
		typedef ptrdiff_t										difference_type;
		typedef T&												reference;
		typedef const T&										const_reference;
		typedef typename Allocator::pointer						pointer;
		typedef typename Allocator::const_pointer				const_pointer;
		typedef typename ft::ra_iterator<value_type>			iterator;
		typedef typename ft::ra_iterator<const value_type>		const_iterator;
		typedef typename ft::reverse_iterator<iterator>			reverse_iterator;
		typedef typename ft::reverse_iterator<const_iterator>	const_reverse_iterator;

		private:
			T*				_data;
			size_type		_size;
			size_type		_capacity;
			allocator_type	_allocator;

		public:
		/*********************************/
		/* Constructors for vector class */
		/*********************************/
			vector();
			vector( const vector& other );
			explicit vector( const Allocator& alloc );
			explicit vector( size_type count,
                 const T& value = T(),
                 const Allocator& alloc = Allocator() );
			template < class InputIt >
			vector( InputIt first, InputIt last,
					const Allocator& alloc = Allocator(), typename ft::enable_if<!ft::is_integral<InputIt>::value, InputIt>::type* = NULL);

		/*******************************/
		/* Destructor for vector class */
		/*******************************/
			virtual	~vector();

		/****************************************/
		/* Assignation methods for vector class */
		/****************************************/
			vector& operator=( const vector& other );

			void assign( size_type count, const T& value );
			template< class InputIt>
			void assign( InputIt first, InputIt last, typename ft::enable_if<!ft::is_integral<InputIt>::value, InputIt>::type* = NULL);
			allocator_type get_allocator() const;

		/******************************/
		/* Accessors for vector class */
		/******************************/
			reference at( size_type pos );
			const_reference at( size_type pos ) const;
			reference operator[]( size_type pos );
			const_reference operator[]( size_type pos ) const;
			reference front();
			const_reference front() const;
			reference back();
			const_reference back() const;
			T* data();
			const T* data() const;

		/******************************************/
		/* Constructors for vector iterator class */
		/******************************************/
			iterator begin();
			const_iterator begin() const;
			iterator end();
			const_iterator end() const;
			reverse_iterator rbegin();
			const_reverse_iterator rbegin() const;
			reverse_iterator rend();
			const_reverse_iterator rend() const;

		/*************************************/
		/* Capacity methods for vector class */
		/*************************************/
			bool empty() const;
			size_type size() const;
			size_type max_size() const;
			void reserve( size_type new_cap );
			size_type capacity() const;

		/*************************************/
		/* Modifier methods for vector class */
		/*************************************/
			void clear();

			iterator insert( iterator pos, const T& value );
			void insert( iterator pos, const size_type& count, const T& value );
			template< class InputIt >
			void insert( iterator pos, InputIt first, InputIt last, typename ft::enable_if<!ft::is_integral<InputIt>::value, InputIt>::type* = NULL);

			iterator erase( iterator pos );
			iterator erase( iterator first, iterator last );

			void push_back( const T& value );
			void pop_back();

			void resize( size_type count, T value = T() );

			void swap( vector& other );
	};

	/***************************************/
	/* Comparison methods for vector class */
	/***************************************/

	template < class T, class Allocator >
	bool operator==( const ft::vector<T, Allocator>& lhs,
						 const ft::vector<T, Allocator>& rhs );

	template < class T, class Allocator >
	bool operator!=( const ft::vector<T, Allocator>& lhs,
						 const ft::vector<T, Allocator>& rhs );

	template < class T, class Allocator >
	bool operator<( const ft::vector<T, Allocator>& lhs,
						const ft::vector<T, Allocator>& rhs );

	template < class T, class Allocator >
	bool operator<=( const ft::vector<T, Allocator>& lhs,
						 const ft::vector<T, Allocator>& rhs );

	template < class T, class Allocator >
	bool operator>( const ft::vector<T, Allocator>& lhs,
						const ft::vector<T, Allocator>& rhs );

	template < class T, class Allocator >
	bool operator>=( const ft::vector<T, Allocator>& lhs,
						 const ft::vector<T, Allocator>& rhs );

	/*****************************************/
	/* Vector specialization for swap method */
	/*****************************************/
	template< class T, class Allocator >
	void swap( ft::vector<T, Allocator>& lhs,
			ft::vector<T, Allocator>& rhs );
}

# include "vector.cpp"
#endif
