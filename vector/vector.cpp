#include "vector.hpp"
#include <iostream>

namespace ft	{
/*********************************/
/* Constructors for vector class */
/*********************************/
	template < class T, class Allocator >
	vector<T, Allocator>::vector()	{
		this->_allocator = Allocator();
		this->_data = this->_allocator.allocate(0);
		this->_size = 0;
		this->_capacity = this->_size;
	}

	template < class T, class Allocator >
	vector<T, Allocator>::vector( const Allocator& alloc )	{
		this->_allocator = alloc;
		this->_data = this->_allocator.allocate(0);
		this->_size = 0;
		this->_capacity = this->_size;
	}

	template < class T, class Allocator >
	vector<T, Allocator>::vector( size_type count,
         const T& value,
         const Allocator& alloc )	{
		if (count > this->max_size())
			throw std::length_error("cannot create ft::vector larger than max_size()");
		this->_allocator = alloc;
		this->_data = this->_allocator.allocate(count);
		this->_size = count;
		this->_capacity = this->_size;
		for (vector<T, Allocator>::iterator it = this->begin(); it < this->end(); it++)
			this->_allocator.construct(&(*it), value);
	}

	template < class T, class Allocator >
	vector<T, Allocator>::vector( const vector& other )	{
		const_iterator	it2 = other.begin();

		this->_allocator = Allocator();
		this->_size = other.size();
		this->_capacity = other.capacity();
		this->_data = this->_allocator.allocate(this->_capacity);
		for (vector<T, Allocator>::iterator it = this->begin();
				it2 < other.end(); it++, it2++)
			this->_allocator.construct(&(*it), *it2);
	}

	template < class T, class Allocator >
	template < class InputIt >
	vector<T, Allocator>::vector( InputIt first, InputIt last,
		const Allocator& alloc, typename ft::enable_if<!ft::is_integral<InputIt>::value, InputIt>::type*)	{
		size_type count = 0;

		for (InputIt it = first; it != last; it++)
			count++;
		if (count > this->max_size())
			throw std::length_error("cannot create ft::vector larger than max_size()");
		this->_allocator = alloc;
		this->_size = count;
		this->_capacity = this->_size;
		this->_data = this->_allocator.allocate(this->_size);
		for (vector<T, Allocator>::iterator it = this->begin();
				first != last; it++, first++)
			this->_allocator.construct(&(*it), *first);
	}

/*******************************/
/* Destructor for vector class */
/*******************************/
	template < class T, class Allocator >
	vector<T, Allocator>::~vector()	{
		for (vector<T, Allocator>::iterator it = this->begin();
				it < this->end(); it++)
			this->_allocator.destroy(&(*it));
		this->_allocator.deallocate(this->_data, this->_capacity);
	}

/****************************************/
/* Assignation methods for vector class */
/****************************************/
	template < class T, class Allocator >
	vector<T, Allocator>& vector<T, Allocator>::operator=( const vector& other )	{
		const_iterator	it2 = other.begin();

		if (this != &other && this->get_allocator() == other.get_allocator())
		{
			for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
				this->_allocator.destroy(&(*it));
			this->_allocator.deallocate(this->_data, this->_capacity);
			this->_allocator = other.get_allocator();
			this->_data = this->_allocator.allocate(other.capacity());
			this->_size = other.size();
			this->_capacity = other.capacity();
			for (vector<T, Allocator>::iterator it = this->begin();
					it2 < other.end(); it++, it2++)
				this->_allocator.construct(&(*it), *it2);
		}
		return (*this);
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::assign( size_type count, const T& value )	{
		if (count > this->max_size())
			throw std::length_error("cannot create ft::vector larger than max_size()");
		for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
			this->_allocator.destroy(&(*it));
		this->_allocator.deallocate(this->_data, this->_capacity);
		this->_data = this->_allocator.allocate(count);
		this->_size = count;
		this->_capacity = this->_size;
		for (vector<T, Allocator>::iterator it = this->begin(); it < this->end(); it++)
			this->_allocator.construct(&(*it), value);
	}

	template < class T, class Allocator >
	template< class InputIt>
	void vector<T, Allocator>::assign( InputIt first, InputIt last, typename ft::enable_if<!ft::is_integral<InputIt>::value, InputIt>::type*)	{
		size_type	count = 0;

		for (InputIt it = first; it != last; it++)
			count++;
		if (count > this->max_size())
			throw std::length_error("cannot create ft::vector larger than max_size()");
		for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
			this->_allocator.destroy(&(*it));
		this->_allocator.deallocate(this->_data, this->_capacity);
		this->_data = this->_allocator.allocate(count);
		this->_size = count;
		this->_capacity = this->_size;
		for (vector<T, Allocator>::iterator it = this->begin();
				first != last; it++, first++)
			this->_allocator.construct(&(*it), *first);
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::allocator_type vector<T, Allocator>::get_allocator() const	{
		return (this->_allocator);
	}


/******************************/
/* Accessors for vector class */
/******************************/
	template < class T, class Allocator >
	typename vector<T, Allocator>::reference vector<T, Allocator>::at( size_type pos )	{
		if (pos < this->_size)
			return (this->_data[pos]);
		else
			throw std::out_of_range("ft::vector error: index is out of bound");
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_reference vector<T, Allocator>::at( size_type pos ) const	{
		if (pos < this->_size)
			return (this->_data[pos]);
		else
			throw std::out_of_range("ft::vector error: index is out of bound");
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::reference vector<T, Allocator>::operator[]( size_type pos )	{
		return (this->at(pos));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_reference vector<T, Allocator>::operator[]( size_type pos ) const	{
		return (this->at(pos));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::reference vector<T, Allocator>::front()	{
		return (this->at(0));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_reference vector<T, Allocator>::front() const	{
		return (this->at(0));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::reference vector<T, Allocator>::back()	{
		return (this->at(this->_size - 1));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_reference vector<T, Allocator>::back() const	{
		return (this->at(this->_size - 1));
	}

	template < class T, class Allocator >
	T* vector<T, Allocator>::data()	{
		return (this->_data);
	}

	template < class T, class Allocator >
	const T* vector<T, Allocator>::data() const	{
		return (this->_data);
	}

/*********************************/
/* Constructors for vector iterator class */
/*********************************/
	template < class T, class Allocator >
	typename vector<T, Allocator>::iterator vector<T, Allocator>::begin()	{
		return (iterator(&(this->_data[0])));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_iterator vector<T, Allocator>::begin() const	{
		return (const_iterator(&(this->_data[0])));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::iterator vector<T, Allocator>::end()	{
		return (iterator(&this->_data[this->_size]));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_iterator vector<T, Allocator>::end() const	{
		return (const_iterator(&this->_data[this->_size]));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::reverse_iterator vector<T, Allocator>::rbegin()	{
		return (reverse_iterator(this->end()));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_reverse_iterator vector<T, Allocator>::rbegin() const	{
		return (const_reverse_iterator(this->end()));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::reverse_iterator vector<T, Allocator>::rend()	{
		return (reverse_iterator(this->begin()));
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::const_reverse_iterator vector<T, Allocator>::rend() const	{
		return (const_reverse_iterator(this->begin()));
	}

/*************************************/
/* Capacity methods for vector class */
/*************************************/
	template < class T, class Allocator >
	bool vector<T, Allocator>::empty() const	{
		if (this->data() == NULL || this->size() == 0)
			return (true);
		return (false);
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::size_type vector<T, Allocator>::size() const	{
		return (this->_size);
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::size_type vector<T, Allocator>::max_size() const	{
		return (this->_allocator.max_size());
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::reserve( size_type new_cap )	{
		value_type	*tmp;
		size_type 	cap = 1;

		if (new_cap <= this->_capacity)
			return ;
		else if (new_cap > this->max_size())
			throw std::length_error("vector::reserve");
		while (cap < new_cap && cap < this->max_size())
		{
			cap <<= 1;
		}
		try
		{
			tmp = this->_allocator.allocate(cap);
		}
		catch (std::exception& e)
		{
			throw e;
		}

		for (size_type i = 0; i < this->_size; i++)
			this->_allocator.construct(&tmp[i], this->_data[i]);
		for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
			this->_allocator.destroy(&(*it));
		this->_allocator.deallocate(this->_data, this->_capacity);
		this->_capacity = cap;
		this->_data = tmp;
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::size_type vector<T, Allocator>::capacity() const	{
		return (this->_capacity);
	}

/*************************************/
/* Modifier methods for vector class */
/*************************************/
	template < class T, class Allocator >
	void vector<T, Allocator>::clear()	{
		for (vector<T, Allocator>::iterator it = this->begin();
				it < this->end(); it++)
			this->_allocator.destroy(&(*it));
		this->_size = 0;
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::iterator vector<T, Allocator>::insert( iterator pos, const T& value )	{
		value_type	*tmp;
		value_type	buf;
		value_type	prev;
		iterator	ret;

		if (this->capacity() > this->size() + 1)
		{
			if (pos != end())
			{
				prev = *pos;
				for (vector<T, Allocator>::iterator it = pos;
						it < (this->end()); it++)
				{
					if ((it + 1) >= this->end())
						this->_allocator.construct(&(*(it + 1)), prev);
					else
					{
						buf = *(it + 1);
						*(it + 1) = prev;
					}
					prev = buf;
				}
				*pos = value;
			}
			else
				this->_allocator.construct(&(*pos), value);
			ret = pos;
		}
		else
		{
			if (this->_size != 0)
			{
				if (this->_size == this->max_size())
					throw std::length_error("cannot create ft::vector larger than max_size()");
				else if (this->_size * 2 > this->max_size())
					tmp = this->_allocator.allocate(this->max_size());
				else
					tmp = this->_allocator.allocate(this->_size * 2);
			}
			else
				tmp = this->_allocator.allocate(1);
			for (vector<T, Allocator>::iterator it(tmp), it2 = this->begin();
					it2 < this->end() || it2 == pos; it++, it2++)
			{
				if (it2 == pos)
				{
					ret = it;
					this->_allocator.construct(&(*it), value);
					it++;
				}
				if (it2 != this->end())
					this->_allocator.construct(&(*it), *it2);
			}
			for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
				this->_allocator.destroy(&(*it));
			this->_allocator.deallocate(this->_data, this->_capacity);
			this->_data = tmp;
			if (this->_size != 0)
			{
				if (this->_size * 2 > this->max_size())
					this->_capacity = this->max_size();
				else
					this->_capacity = this->_size * 2;
			}
			else
				this->_capacity = 1;
		}
		this->_size += 1;
		return (ret);
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::insert( iterator pos, const size_type& count, const T& value )	{
		value_type	*tmp;
		size_type			tot = 0;

		if (this->capacity() > this->size() + count)
		{
			for (vector<T, Allocator>::iterator it = this->end() + count;
					it > this->begin() && (it - count) >= this->begin(); it--)
			{
				if (it >= this->end() && it - count < this->end())
					this->_allocator.construct(&(*it), *(it - count));
				else if (it - count < this->end())
					*it = *(it - count);
			}
			for (size_type i = 0;
					i < count; i++)
			{
					*pos = value;
					pos++;
			}
		}
		else
		{
			if (this->_size + count > this->max_size())
				throw std::length_error("cannot create ft::vector larger than max_size()");
			else
				tmp = this->_allocator.allocate(this->_size + count);
			for (vector<T, Allocator>::iterator it(tmp), it2 = this->begin();
					it2 <= this->end() && tot < this->_size + count; it++, it2++)
			{
				if (it2 == pos)
				{
					for (size_type i = 0; i < count; i++)
					{
						this->_allocator.construct(&(*it), value);
						it++;
					}
				}
				if (it2 != this->end())
					this->_allocator.construct(&(*it), *it2);
				tot++;
			}
			for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
				this->_allocator.destroy(&(*it));
			this->_allocator.deallocate(this->_data, this->_capacity);
			this->_data = tmp;
			this->_capacity = this->_size + count;
		}
		this->_size += count;
	}

	template < class T, class Allocator >
	template< class InputIt >
	void vector<T, Allocator>::insert( iterator pos, InputIt first, InputIt last, typename ft::enable_if<!ft::is_integral<InputIt>::value, InputIt>::type*)	{
		size_type	count;
		value_type	*tmp;
		
		count = 0;
		for (InputIt it = first; it != last; it++)
			count++;
		if (this->capacity() > this->size() + count)
		{
			for (vector<T, Allocator>::iterator it = pos;
					it < (this->end() + count); it++)
			{
				if ((it + count) >= this->end())
					this->_allocator.construct(&(*(it + count)), *it);
				else
					*(it + count) = *it;
			}
			for (vector<T, Allocator>::iterator it = pos;
					first != last; it++, first++)
					*it = *first;
		}
		else
		{
			if (this->_size + count > this->max_size())
				throw std::length_error("cannot create ft::vector larger than max_size()");
			else
				tmp = this->_allocator.allocate(this->_size + count);
			for (vector<T, Allocator>::iterator it(tmp), it2 = this->begin();
					it2 < this->end() || it2 == pos; it++, it2++)
			{
				if (it2 == pos)
				{
					while (first != last)
					{
						this->_allocator.construct(&(*it), *first);
						first++;
						it++;
					}
				}
				if (it2 != this->end())
					this->_allocator.construct(&(*it), *it2);
			}
			for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
				this->_allocator.destroy(&(*it));
			this->_allocator.deallocate(this->_data, this->_capacity);
			this->_data = tmp;
			this->_capacity = this->_size + count;
		}
		this->_size += count;
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::iterator vector<T, Allocator>::erase( iterator pos )	{
		if (pos < this->begin() || pos >= this->end())
			return (pos);
		for (vector<T, Allocator>::iterator it = pos; it + 1 < this->end(); it++)
			*it = *(it + 1);
		this->_allocator.destroy(&(*(this->end() - 1)));
		this->_size -= 1;
		return (pos);
	}

	template < class T, class Allocator >
	typename vector<T, Allocator>::iterator vector<T, Allocator>::erase( iterator first, iterator last )	{
		size_type	count;

		count = last - first;
		for (vector<T, Allocator>::iterator it = first; it < this->end(); it++)
		{
			if (it + count < this->end())
				*it = *(it + count);
			else
				this->_allocator.destroy(&(*it));
		}
		this->_size -= count;
		return (first);
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::push_back( const T& value )	{
		value_type	*tmp;

		if (this->capacity() > this->size() + 1)
		{
			this->_allocator.construct(&(*(this->end())), value);
		}
		else
		{
			if (this->_size != 0)
			{
				if (this->_size == this->max_size())
					throw std::length_error("cannot create ft::vector larger than max_size()");
				else if (this->_size * 2 > this->max_size())
					tmp = this->_allocator.allocate(this->max_size());
				else
					tmp = this->_allocator.allocate(this->_size * 2);
			}
			else
				tmp = this->_allocator.allocate(1);
			for (vector<T, Allocator>::iterator it(tmp), it2 = this->begin();
					it2 < this->end(); it++, it2++)
				this->_allocator.construct(&(*it), *it2);
			this->_allocator.construct(&tmp[this->_size], value);
			for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
				this->_allocator.destroy(&(*it));
			this->_allocator.deallocate(this->_data, this->_capacity);
			this->_data = tmp;
			if (this->_size != 0)
			{
				if (this->_size * 2 > this->max_size())
					this->_capacity = this->max_size();
				else
					this->_capacity = this->_size * 2;
			}
			else
				this->_capacity = 1;
		}
		this->_size += 1;
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::pop_back()	{
		if (this->_size != 0)
		{
			this->_allocator.destroy(&(*(this->end() - 1)));
			this->_size -= 1;
		}
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::resize( size_type count, T value)	{
		value_type	*tmp;

		if (this->_size < count)
		{
			if (this->_capacity >= this->_size + count)
			{
				for (vector<T, Allocator>::iterator it = this->end();
						it < this->end() + count; it++)
					this->_allocator.construct(&(*it), value);
			}
			else
			{
				if (count > this->max_size())
					throw std::length_error("cannot create ft::vector larger than max_size()");
				tmp = this->_allocator.allocate(count);
				for (vector<T, Allocator>::iterator it(tmp), it2 = this->begin();
						it2 < this->end(); it++, it2++)
					this->_allocator.construct(&(*it), *it2);
				for (vector<T, Allocator>::iterator it(&tmp[this->_size]);
						it < &tmp[count]; it++)
					this->_allocator.construct(&(*it), value);
				for (vector<T, Allocator>::iterator	it = this->begin(); it < this->end(); it++)
					this->_allocator.destroy(&(*it));
				this->_allocator.deallocate(this->_data, this->_capacity);
				this->_data = tmp;
				this->_capacity = count;
			}
		}
		else
		{
			for (vector::iterator it = &(this->_data[count]); it != this->end(); it++)
				this->_allocator.destroy(&(*it));
		}
		this->_size = count;
	}

	template < class T, class Allocator >
	void vector<T, Allocator>::swap( vector& other )	{
		T	*tmp = this->data();
		size_type	size_buf = this->size();
		size_type	cap_buf = this->capacity();

		if (this->get_allocator() == other.get_allocator())
		{
			this->_data = other._data;
			this->_size = other._size;
			this->_capacity = other._capacity;
			other._data = tmp;
			other._size = size_buf;
			other._capacity = cap_buf;
		}
	}

/***************************************/
/* Comparison methods for vector class */
/***************************************/
	template < class T, class Allocator >
	bool operator==( const ft::vector<T, Allocator>& lhs,
							 const ft::vector<T, Allocator>& rhs )
	{
		return (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template < class T, class Allocator >
	bool operator!=( const ft::vector<T, Allocator>& lhs,
							 const ft::vector<T, Allocator>& rhs )
	{
		return (!(lhs.size() == rhs.size()) || !ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template < class T, class Allocator >
	bool operator<( const ft::vector<T, Allocator>& lhs,
							const ft::vector<T, Allocator>& rhs )
	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template < class T, class Allocator >
	bool operator<=( const ft::vector<T, Allocator>& lhs,
							 const ft::vector<T, Allocator>& rhs )
	{
		if (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()))
			return (true);
		else if (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()))
			return (true);
		else
			return (false);
	}

	template < class T, class Allocator >
	bool operator>( const ft::vector<T, Allocator>& lhs,
							const ft::vector<T, Allocator>& rhs )
	{
		if (!ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		  && (lhs.size() > rhs.size() || !ft::equal(lhs.begin(), lhs.end(), rhs.begin())))
			return (true);
		else
			return (false);
	}

	template < class T, class Allocator >
	bool operator>=( const ft::vector<T, Allocator>& lhs,
							 const ft::vector<T, Allocator>& rhs )
	{
		if (!ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		  && (lhs.size() > rhs.size() || !ft::equal(lhs.begin(), lhs.end(), rhs.begin())))
			return (true);
		else if (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()))
			return (true);
		else
			return (false);
	}

/*****************************************/
/* Vector specialization for swap method */
/*****************************************/
	template< class T, class Allocator >
	void swap( ft::vector<T, Allocator>& lhs,
			ft::vector<T, Allocator>& rhs )
	{
		lhs.swap(rhs);
	}
}
