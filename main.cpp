#include <unistd.h>
#include <sys/wait.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <string>
#include "tester/test_utils.hpp"


#define DIFF_LOG "/diff/"

#define MAKE "/usr/bin/make"
#define DIR_FLAG "-sC"
#define RE "ft=0"
#define STD "ft=1"
#define FCLEAN "fclean"

int	build_ft(const char *set[], char **env)
{
	pid_t	pid;
	struct timeval	begin;
	struct timeval end;
	const char *exec[] = {"./set/set", "./stack/stack", "./map/map", "./vector/vector", NULL};
	char **p;
	int	ret;
	double	diff_time;

	p = (char**)malloc(5 * sizeof(char*));
	for (int i = 0; set[i] != NULL; i++)
	{
		p[0] = (char*)MAKE;
		p[1] = (char*)DIR_FLAG;
		p[2] = (char*)set[i];
		p[3] = (char*)RE;
		p[4] = NULL;
		pid = fork();
		if (pid == 0)
		{
			ret = execve(p[0], p, env);
			if (ret == -1)
				exit(1);
		}
		else
		{
			waitpid(pid, &ret, 0);
			if (!WEXITSTATUS(ret) || WIFSIGNALED(ret))
			{
				std::cout << "\e[032mft::" << set[i]
							<< " was succesfully built !\e[0m" << std::endl;
			}
			else
			{
				std::cout << "\e[031mFailed to build ft::"
							<< set[i] << " !\e[0m" << std::endl;
			}
			p[0] = (char*)exec[i];
			p[1] = NULL;
			p[1] = NULL;
			p[2] = NULL;
			p[3] = NULL;
			p[4] = NULL;
			gettimeofday(&begin, NULL);
			pid = fork();
			if (pid == 0)
			{
				ret = execve(p[0], p, env);
				if (ret == -1)
					exit(1);
			}
			else
			{
				waitpid(pid, &ret, 0);
				gettimeofday(&end, NULL);
				if (!WEXITSTATUS(ret) || WIFSIGNALED(ret))
				{
					diff_time = ((end.tv_sec * 1000
							+ (end.tv_usec / 1000))
							- ((begin.tv_sec * 1000)
							+ (begin.tv_usec / 1000)));
					std::cout << std::fixed << std::setprecision(3)
							<< exec[i] << " executed in "
							<< diff_time << "ms" << std::endl;
				}
				else
				{
					std::cout << "\e[031mCouldn't execute "
								<< exec[i] << "\e[0m" << std::endl;
				}
			}
		}
	}
	free(p);
	return (0);
}

int	build_std(const char *set[], char **env)
{
	pid_t	pid;
	struct timeval	begin;
	struct timeval end;
	const char *std_exec[] = {"./set/std_set", "./stack/std_stack", "./map/std_map", "./vector/std_vector", NULL};
	char **p;
	int	ret;
	double	diff_time;

	p = (char**)malloc(6 * sizeof(char*));
	for (int i = 0; set[i] != NULL; i++)
	{
		p[0] = (char*)MAKE;
		p[1] = (char*)DIR_FLAG;
		p[2] = (char*)set[i];
		p[3] = (char*)STD;
		p[4] = NULL;
		pid = fork();
		if (pid == 0)
		{
			ret = execve(p[0], p, env);
			if (ret == -1)
				exit(1);
		}
		else
		{
			waitpid(pid, &ret, 0);
			if (!WEXITSTATUS(ret) || WIFSIGNALED(ret))
			{
				std::cout << "\e[032mstd::" << set[i]
							<< " was succesfully built !\e[0m" << std::endl;
			}
			else
			{
				std::cout << "\e[031mFailed to build std::" << set[i]
							<< " !\e[0m" << std::endl;
			}
			p[0] = (char*)std_exec[i];
			p[1] = NULL;
			p[2] = NULL;
			p[3] = NULL;
			p[4] = NULL;
			gettimeofday(&begin, NULL);
			pid = fork();
			if (pid == 0)
			{
				ret = execve(p[0], p, env);
				if (ret == -1)
					exit(1);
			}
			else
			{
				waitpid(pid, &ret, 0);
				gettimeofday(&end, NULL);
				if (!WEXITSTATUS(ret) || WIFSIGNALED(ret))
				{
					diff_time = ((end.tv_sec * 1000
							+ (end.tv_usec / 1000))
							- ((begin.tv_sec * 1000)
							+ (begin.tv_usec / 1000)));
					std::cout << std::fixed << std::setprecision(3)
							<< std_exec[i] << " executed in "
							<< diff_time << "ms" << std::endl;
				}
				else
				{
					std::cout << "\e[031mCouldn't execute "
								<< std_exec[i] << "\e[0m" << std::endl;
				}
			}
		}
	}
	free(p);
	return (0);
}

int	clean(const char *set[], char **env)
{
	pid_t	pid;
	char **p;
	int	ret;

	p = (char**)malloc(5 * sizeof(char*));
	for (int i = 0; set[i] != NULL; i++)
	{
		p[0] = (char*)MAKE;
		p[1] = (char*)DIR_FLAG;
		p[2] = (char*)set[i];
		p[3] = (char*)FCLEAN;
		p[4] = NULL;
		pid = fork();
		if (pid == 0)
		{
			ret = execve(p[0], p, env);
			if (ret == -1)
				exit(1);
		}
		else
		{
			waitpid(pid, &ret, 0);
		}
	}
	free(p);
	return (0);
}

int	f_comp(const char *container, std::string s1, std::string s2)
{
	int				i = 0;
	std::string		line1;
	std::string		line2;
	std::string		path1 = container;;
	std::string		path2 = container;
	bool			diff = 0;
	std::ofstream	diff_log;

	path1 += "/logs/";
	path2 += "/logs/";
	path1 += s1;
	path2 += s2;
	std::string		diff_path = container;
	diff_path += DIFF_LOG;
	diff_path += s1.replace(0, 3, "diff_");
	std::ifstream	f1(path1.c_str());
	std::ifstream	f2(path2.c_str());

	while (std::getline(f1, line1) && std::getline(f2, line2))
	{
		if (line1.compare(line2))
		{
			if (diff == 0)
			{
				diff_log.open(diff_path.c_str());
				diff_log << "Diff in file " << path1 << std::endl;
				diff = 1;
			}
			diff_log << "diff on line " << i << std::endl;
			diff_log << ">" << line1 << std::endl << std::endl;
			diff_log << "<" << line2 << std::endl << std::endl;
		}
		i++;
	}
	if (diff == 1)
		diff_log.close();
	return (0);
}

int	diff_both(const char *container)//, char **env)
{
	std::string	path = container;
	path += "/logs";
	std::vector<std::string> plop = search_path(path.c_str());
	for (std::vector<std::string>::size_type i = 0; i < plop.size() / 2; i++)
	{
		f_comp(container, plop[i], plop[i + plop.size() / 2]);
	}	
	return (0);
}

int main(int ac, char **av, char **env)
{
	const char *set[] =  {"set", "stack", "map", "vector", NULL};

	(void)ac;
	(void)av;
	build_ft(set, env);
	clean(set, env);
	build_std(set, env);
	for (int i = 0; set[i]; i++)
		diff_both(set[i]);//, env);
	clean(set, env);
	return (0);
}
