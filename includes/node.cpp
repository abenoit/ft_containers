#include "node.hpp"
#include <iostream>

namespace	ft	{
	template <typename T, typename Alloc>
	node<T, Alloc>::node()	{
		this->_content = NULL;
		this->_parent = NULL;
		this->_left = NULL;
		this->_right = NULL;
		this->_color = RED;
	}

	template <typename T, typename Alloc>
	node<T, Alloc>::node(const node& ref)
	{
		pointer	tmp = this->_alloc.allocate(1, NULL);

		this->_alloc.construct(tmp, ref.getContent());
		this->_content = tmp;
		this->_parent = ref.getParent();
		this->_left = ref.getLeft();
		this->_right = ref.getRight();
		this->_color = ref.getColor();
	}

	template <typename T, typename Alloc>
	node<T, Alloc>::node(const value_type& content)
	{
		pointer	tmp = this->_alloc.allocate(1, NULL);

		this->_alloc.construct(tmp, content);
		this->_content = tmp;
		this->_parent = NULL;
		this->_left = NULL;
		this->_right = NULL;
		this->_color = RED;
	}

	template <typename T, typename Alloc>
	node<T, Alloc>::~node()	{
		this->_alloc.destroy(this->_content);
		this->_alloc.deallocate(this->_content, 1);
	}

template <typename T, typename Alloc>
	node<T, Alloc> & node<T, Alloc>::operator=(node &ref)	{
		this->_content = ref.getContent();
		this->_left = ref.getLeft();
		this->_right = ref.getRight();
		this->_color = ref.getColor();
		return (*this);
	}

	template <typename T, typename Alloc>
	typename node<T, Alloc>::value_type&	node<T, Alloc>::getContent()	{
		return (*this->_content);
	}

	template <typename T, typename Alloc>
	const typename node<T, Alloc>::value_type&	node<T, Alloc>::getContent()	const	{
		return (*this->_content);
	}

	template <typename T, typename Alloc>
	node<T, Alloc> *	node<T, Alloc>::getRoot()	{
		node<T, Alloc>*	ptr;

		ptr = this;
		if (ptr->getParent() == NULL)
			return (ptr);
		else
			ptr = ptr->getParent();
		return (ptr);
	}

	template <typename T, typename Alloc>
	node<T, Alloc> *	node<T, Alloc>::getParent()	const {
		return (this->_parent);
	}

	template <typename T, typename Alloc>
	node<T, Alloc> *	node<T, Alloc>::getLeft()	const {
		return (this->_left);
	}

	template <typename T, typename Alloc>
	node<T, Alloc> *	node<T, Alloc>::getRight()	const {
		return (this->_right);
	}

	template <typename T, typename Alloc>
	node<T, Alloc> *	node<T, Alloc>::getGrandParent()	const {
		node<T, Alloc>	*tmp;

		if ((tmp = this->getParent()) == NULL)
			return (NULL);
		return (tmp->getParent());
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::getAuntie()	const {
		node<T, Alloc>*	ptr = this->getParent();

		if (ptr == NULL)
			return (NULL);
		if (this->getGrandParent() == NULL)
			return (NULL);
		if (this->getGrandParent()->getLeft() != ptr)
			return (this->getGrandParent()->getLeft());
		else
			return (this->getGrandParent()->getRight());
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::getSibling()	const {
		node<T, Alloc>*	ptr = this->getParent();

		if (ptr == NULL)
			return (NULL);
		if (ptr->getLeft() != this)
			return (ptr->getLeft());
		else
			return (ptr->getRight());
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::getCloseNephew()	const {
		node<T, Alloc>*	ptr = this->getSibling();

		if (ptr == NULL)
			return (NULL);
		if (this->getParent()->getLeft() == this)
			return (ptr->getLeft());
		else
			return (ptr->getRight());
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::getDistantNephew()	const {
		node<T, Alloc>*	ptr = this->getSibling();

		if (ptr == NULL)
			return (NULL);
		if (this->getParent()->getLeft() == this)
			return (ptr->getRight());
		else
			return (ptr->getLeft());
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::getPredecessor()	const {
		node<T, Alloc>*	ptr = this->getLeft();

		while (ptr != NULL && ptr->getRight() != NULL)
			ptr = ptr->getRight();
		return (ptr);
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::getSuccessor()	const {
		node<T, Alloc>*	ptr = this->getRight();

		while (ptr != NULL && ptr->getLeft() != NULL)
			ptr = ptr->getLeft();
		return (ptr);
	}

	template <typename T, typename Alloc>
	bool	node<T, Alloc>::getColor()	const {
		return (this->_color);
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::setParent(node *parent)	{
		this->_parent = parent;
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::setLeft(node *left)	{
		this->_left = left;
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::setRight(node *right)	{
		this->_right = right;
	}

	template <typename T, typename Alloc>
	int	node<T, Alloc>::balance(node **head)	{
		node<T, Alloc>*	ptr;

		if (this->getParent() == NULL || this->getParent()->getColor() == BLACK)
			return (0);
		if (this->getGrandParent() == NULL)
		{
			this->getParent()->recolor(BLACK);
			return (0);
		}
		if (this->getAuntie() == NULL || this->getAuntie()->getColor() == BLACK)
		{
			if (this->getGrandParent()->getRight() == this->getParent()
					&& this->getParent()->getLeft() == this)
			{
				ptr = this->getParent();
				ptr->rotate_right(head);
				return (ptr->balance2(head));
			}
			else if (this->getGrandParent()->getLeft() == this->getParent()
					&& this->getParent()->getRight() == this)
			{
				ptr = this->getParent();
				ptr->rotate_left(head);
				return (ptr->balance2(head));
			}
			return (this->balance2(head));
		}
		else
			return (this->balance3(head));
	}

	template <typename T, typename Alloc>
	int	node<T, Alloc>::balance2(node **head)	{
		this->getGrandParent()->recolor(RED);
		this->getParent()->recolor(BLACK);
		if (this->getGrandParent()->getRight() == this->getParent()
				&& this->getParent()->getRight() == this)
		{
			this->getGrandParent()->rotate_left(head);
		}
		else if (this->getGrandParent()->getLeft() == this->getParent()
				&& this->getParent()->getLeft() == this)
		{
			this->getGrandParent()->rotate_right(head);
		}
		return (0);
	}	

	template <typename T, typename Alloc>
	int	node<T, Alloc>::balance3(node **head)	{
		if (this->getParent() != NULL)
			this->getParent()->recolor(BLACK);
		if (this->getAuntie() != NULL)
			this->getAuntie()->recolor(BLACK);
		if (this->getGrandParent() != NULL)
		{
			this->getGrandParent()->recolor(RED);
			return (this->getGrandParent()->balance(head));
		}
		return (this->balance(head));
	}

	template <typename T, typename Alloc>
 	node<T, Alloc>*	node<T, Alloc>::find(const value_type& val)	{
		node<T, Alloc>	*ptr;

		if (val == this->getContent())
			return (this);
		else
		{
			if (this->getLeft() != NULL)
				ptr = this->getLeft()->find(val);
			if (ptr != NULL)
				return (ptr);
			if (this->getRight() != NULL)
				return (this->getRight()->find(val));
			return (NULL);
		}
	}

	template <typename T, typename Alloc>
	int	node<T, Alloc>::nodeOut()	{
		std::cout << *this;
		return (0);
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::color_switch()	{
		if (this->getColor() == BLACK)
			this->_color = RED;
		else
			this->_color = BLACK;
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::recolor(bool color)	{
		this->_color = color;
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::rotate_right(node<T, Alloc>** root)	{
		if (this->getLeft() != NULL)
		{
			this->getLeft()->setParent(this->getParent());
			if (this->getParent() == NULL)
				*root = this->getLeft();
			else if (this->getParent()->getRight() == this)
				this->getParent()->setRight(this->getLeft());
			else
				this->getParent()->setLeft(this->getLeft());
			this->setParent(this->getLeft());
			this->setLeft(this->getParent()->getRight());
			if (this->getLeft() != NULL)
				this->getLeft()->setParent(this);
			this->getParent()->setRight(this);
		}
	}

	template <typename T, typename Alloc>
	void	node<T, Alloc>::rotate_left(node<T, Alloc>** root)	{
		if (this->getRight() != NULL)
		{
			this->getRight()->setParent(this->getParent());
			if (this->getParent() == NULL)
				*root = this->getRight();
			else if (this->getParent()->getRight() == this)
				this->getParent()->setRight(this->getRight());
			else
				this->getParent()->setLeft(this->getRight());
			this->setParent(this->getRight());
			this->setRight(this->getParent()->getLeft());
			if (this->getRight() != NULL)
				this->getRight()->setParent(this);
			this->getParent()->setLeft(this);
		}
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::first()	{
		node*	ptr = this;

		if (ptr != NULL && ptr->getParent() != NULL)
		{
			while (ptr->getParent() != NULL && ptr->getParent()->getRight() == ptr)
				ptr = ptr->getParent();
		}
		while (ptr != NULL && ptr->getLeft() != NULL)
		{
			ptr = ptr->getLeft();
		}
		return (ptr);
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::last()	{
		node*	ptr = this;

		if (ptr->getParent() != NULL)
		{
			while (ptr->getParent() != NULL && ptr->getParent()->getLeft() == ptr)
				ptr = ptr->getParent();
		}
		while (ptr != NULL && ptr->getRight() != NULL)
			ptr = ptr->getRight();
		return (ptr);
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::next()	{
		node<T, Alloc>*	ptr;

		if (this->getRight() != NULL)
		{
			ptr = this->getRight();
			while (ptr->getLeft() != NULL)
				ptr = ptr->getLeft();
			return (ptr);
		}
		if (this->getParent() == NULL)
			return (NULL);
		if (this->getParent()->getRight() == this)
		{
			if (this->getGrandParent() != NULL)
			{
				ptr = this->getParent();
				while (ptr->getParent() != NULL)
				{
					if (ptr->getParent()->getLeft() == ptr)
						return (ptr->getParent());
					ptr = ptr->getParent();
				}
				return (NULL);				
			}
			return (NULL);
		}
		else
		{
			return (this->getParent());
		}
	}

	template <typename T, typename Alloc>
	node<T, Alloc>*	node<T, Alloc>::previous()	{
		node<T, Alloc>*	ptr;

		if (this->getLeft() != NULL)
		{
			ptr = this->getLeft();
			while (ptr->getRight() != NULL)
				ptr = ptr->getRight();
			return (ptr);
		}
		if (this->getParent() == NULL)
			return (NULL);
		if (this->getParent()->getLeft() == this)
		{
			if (this->getGrandParent() != NULL)
			{
				ptr = this->getParent();
				while (ptr->getParent() != NULL)
				{
					if (ptr->getParent()->getRight() == ptr)
						return (ptr->getParent());
					ptr = ptr->getParent();
				}
				return (NULL);				
			}
			return (NULL);
		}
		else
		{
			return (this->getParent());
		}
	}

	template <typename T, typename Alloc>
	int	node<T, Alloc>::maxDepth(int i)	{
		int	ldepth;
		int	rdepth;

		ldepth = i;
		rdepth = i;
		if (this->getLeft() != NULL)
			ldepth = this->getLeft()->maxDepth(i + 1);
		if (this->getRight() != NULL)
			rdepth = this->getRight()->maxDepth(i + 1);
		return (ldepth > rdepth ? ldepth : rdepth);
	}

	template <typename T, typename Alloc>
	int	node<T, Alloc>::blackDepth(int i)	{
		int	ldepth;
		int	rdepth;

		if (this->getColor() == BLACK)
			i += 1;
		ldepth = i;
		rdepth = i;
		if (this->getLeft() != NULL)
			ldepth = this->getLeft()->blackDepth(i);
		if (this->getRight() != NULL)
			rdepth = this->getRight()->blackDepth(i);
		return (ldepth > rdepth ? ldepth : rdepth);
	}

	template <typename T, typename Alloc>
	node<T, Alloc>& node<T, Alloc>::operator*()	const {
		return (this->getContent());
	}

	template <typename T, typename Alloc>
	std::ostream & operator<<(std::ostream & o, node<T, Alloc> &ref)	{
		o << "(" << ref.getContent().first << "," << ref.getContent().second << ")";
		return (o);
	}
}
