#ifndef FT_UTILS_HPP
# define FT_UTILS_HPP

#include "iterators.hpp"

namespace	ft	{
	/***************/
	/* Type traits */
	/***************/
	template<class T, T v>
	struct integral_constant {
		static const T value = v;
	};

	typedef	integral_constant<bool, true> true_type;
	typedef	integral_constant<bool, false> false_type;

	template <bool, typename T = void>
	struct enable_if
	{};

	template <typename T>
	struct enable_if<true, T> {
	  typedef T type;
	};

	template <typename iterator_category = NAMESPACE::bidirectional_iterator_tag>
	struct	is_vector	: ft::false_type {};

	template <>
	struct	is_vector<NAMESPACE::random_access_iterator_tag>	: ft::true_type {};

	template< class T >
	struct is_integral : ft::false_type {};

	template <>
	struct	is_integral<bool>	: ft::true_type {};
	template <>
	struct	is_integral<char>	: ft::true_type {};
	template <>
	struct	is_integral<wchar_t>	: ft::true_type {};
	template <>
	struct	is_integral<short>	: ft::true_type {};
	template <>
	struct	is_integral<int>	: ft::true_type {};
	template <>
	struct	is_integral<long>	: ft::true_type {};
	template <>
	struct	is_integral<long long>	: ft::true_type {};

	/************************/
	/* Comparison templates */
	/************************/
	template< class InputIt1, class InputIt2 >
	bool equal( InputIt1 first1, InputIt1 last1,
				InputIt2 first2 )
	{
		while (first1 != last1)
		{
			if (!(*first1 == *first2))
				return (false);
			++first1;
			++first2;
		}
		return (true);
	}

	template< class InputIt1, class InputIt2, class BinaryPredicate >
	bool equal( InputIt1 first1, InputIt1 last1,
				InputIt2 first2, BinaryPredicate p )
	{
		while (first1 != last1)
		{
			if (p(*first1, *first2))
				return (false);
			++first1;
			++first2;
		}
		return (true);
	}

	template< class InputIt1, class InputIt2 >
	bool lexicographical_compare( InputIt1 first1, InputIt1 last1,
								  InputIt2 first2, InputIt2 last2 )
	{
		while (first1 != last1)
		{
			if (first2 == last2 || *first2 < *first1)
				return (false);
			else if (*first1 < *first2)
				return (true);
			first1++;
			first2++;
		}
		return (first2 != last2);
	}

	template< class InputIt1, class InputIt2, class Compare >
	bool lexicographical_compare( InputIt1 first1, InputIt1 last1,
								  InputIt2 first2, InputIt2 last2,
								  Compare comp )
	{
		while (first1 != last1)
		{
			if (first2 == last2 || comp(*first2, *first1))
				return (false);
			else if (comp(*first1, *first2))
				return (true);
			first1++;
			first2++;
		}
		return (first2 != last2);
	}
}
#endif
