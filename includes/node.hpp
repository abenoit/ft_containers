#ifndef NODE_HPP
# define NODE_HPP

# define BLACK	0
# define RED	1

#include "pair.hpp"

namespace	ft	{
	template <typename T, typename Alloc = std::allocator<T> >
	struct	node	{
			typedef T				value_type;
			typedef	std::size_t								size_type;
			typedef	std::ptrdiff_t							difference_type;
			typedef	Alloc									allocator_type;
			typedef	value_type&								reference;
			typedef	const value_type&						const_reference;
			typedef typename Alloc::pointer					pointer;
			typedef	typename Alloc::const_pointer			const_pointer;

			bool			_color;
			pointer			_content;
			node			*_parent;
			node			*_left;
			node			*_right;
			allocator_type	_alloc;

			node();
			node(const value_type& content);
			node(const node& ref);
			virtual	~node();

			value_type&	getContent();
			const value_type&	getContent() const;
			node*	getRoot() ;
			node*	getParent() const ;
			node*	getLeft() const ;
			node*	getRight() const ;
			node*	getGrandParent() const ;
			node*	getAuntie() const ;
			node*	getSibling() const ;
			node*	getCloseNephew() const ;
			node*	getDistantNephew() const ;
			node*	getPredecessor() const ;
			node*	getSuccessor() const ;
			bool	getColor() const;
			void	setParent(node*);
			void	setLeft(node*);
			void	setRight(node*);

			int	balance(node **head);
			int	balance2(node **head);
			int	balance3(node **head);
			int	nodeOut();
		 	node*	find(const value_type& val);

			void	color_switch();
			void	recolor(bool color);
			void	rotate_right(node** root);
			void	rotate_left(node**	root);

			node*	first();
			node*	last();

			node*	next();
			node*	previous();

			int	maxDepth(int i);
			int	blackDepth(int i);

			node & operator=(node &ref);
			node & operator*()	const;
	};

	template <typename T, typename Alloc>
	std::ostream & operator<<(std::ostream & o, node<T, Alloc> &ref);
}

#include "node.cpp"
#endif
