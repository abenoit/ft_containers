#ifndef	ITERATORS_HPP
# define ITERATORS_HPP

# include <iostream>
# include "node.hpp"

namespace	ft	{
	template <typename T, bool>
	struct is_const	{
		typedef T	type;
	};

	template <typename T>
	struct is_const<T, true>	{
		typedef const	T	type;
	};

	template <typename T>
	struct remove_const	{
		typedef T	type;
	};

	template <typename T>
	struct remove_const<const T>	{
		typedef T	type;
	};

	/********************/
	/* Iterators traits */
	/********************/
	struct iterator_tag	{ };
	struct input_iterator_tag : public iterator_tag { };
	struct output_iterator_tag : public iterator_tag { };
	struct forward_iterator_tag : public input_iterator_tag { };
	struct bidirectional_iterator_tag : public forward_iterator_tag { };
	struct random_access_iterator_tag : public bidirectional_iterator_tag { };

	template<
		class Category,
		class T,
		class Distance = std::ptrdiff_t,
		class Pointer = T*,
		class Reference = T&>
	struct iterator
	{
		typedef T				value_type;
		typedef Distance		difference_type;
		typedef Pointer			pointer;
		typedef Reference		reference;
		typedef Category		iterator_category;

		iterator() {};
		virtual	~iterator() {};
	};

	class	output_iterator : public iterator< output_iterator_tag, void, std::ptrdiff_t, void, void > {};

	template<class T>
	class	input_iterator : public iterator< input_iterator_tag, T> {};

	template<class T>
	class	forward_iterator : public iterator< forward_iterator_tag, T> {};

	template<class T>
	class	bidirectional_iterator : public iterator< bidirectional_iterator_tag, T> {};

	template<class T>
	class	random_access_iterator : public iterator< random_access_iterator_tag, T> {};


	template< class Iter >
	struct iterator_traits	{
		typedef typename Iter::difference_type 		difference_type;
		typedef typename Iter::value_type			value_type;
		typedef typename Iter::pointer				pointer;
		typedef typename Iter::reference			reference;
		typedef typename Iter::iterator_category	iterator_category;
	};

	template< class T >
	struct iterator_traits<T*>	{
		typedef typename std::ptrdiff_t 					difference_type;
		typedef T				value_type;
		typedef value_type*											pointer;
		typedef value_type&											reference;
		typedef typename ft::iterator_tag		iterator_category;
	};

	template< class T >
	struct iterator_traits<const T*>	{
		typedef typename std::ptrdiff_t					difference_type;
		typedef T			value_type;
		typedef const value_type*								pointer;
		typedef const value_type&								reference;
		typedef typename ft::iterator_tag	iterator_category;
	};

	template <typename T>
	struct is_iterator {  
		template <typename U>
		static char test(typename ft::iterator_traits<U>::pointer* x);

		template <typename U>
		static long test(U* x);

		static const bool value = sizeof(test<T>(NULL)) == 1;
	};

	template <typename T, bool constness = false>
	struct	bd_iterator	: public ft::bidirectional_iterator<T>	{
		typedef	typename ft::bidirectional_iterator<typename ft::is_const<T, constness>::type >	iterator_type;
		typedef typename ft::iterator_traits<iterator_type>::difference_type 		difference_type;
		typedef typename ft::iterator_traits<iterator_type>::value_type				value_type;
		typedef typename ft::iterator_traits<iterator_type>::pointer				pointer;
		typedef typename ft::iterator_traits<iterator_type>::reference				reference;
		typedef typename ft::iterator_traits<iterator_type>::iterator_category		iterator_category;
		typedef typename ft::node<typename remove_const<value_type>::type >										node_type;
		typedef	node_type*															node_pointer;

		protected:
			node_pointer	m_ptr;
			node_pointer	_next;
			node_pointer	_previous;

		public:
			bd_iterator()	:	m_ptr(NULL), _next(NULL), _previous(NULL) {};
			bd_iterator(node_pointer ptr)	:	m_ptr(ptr), _next(ptr != NULL ? ptr->next(): NULL), _previous(ptr != NULL ? ptr->previous(): NULL) {};

			bd_iterator(const bd_iterator& other)	:	m_ptr(other.base_ptr()), _next(other.next_ptr()), _previous(other.previous_ptr()) {};

			template <typename U>
			bd_iterator(const bd_iterator<U, false>& other)	:	m_ptr(other.base_ptr()), _next(other.next_ptr()), _previous(other.previous_ptr()) {};

			virtual ~bd_iterator()	{};
			reference operator*() const { return *this->m_ptr->_content; }
			pointer operator->() const { return this->m_ptr->_content; }

			node_pointer	base_ptr()	const { return (m_ptr); }
			node_pointer	next_ptr()	const { return (_next); }
			node_pointer	previous_ptr()	const { return (_previous); }
			const bd_iterator & operator= (const bd_iterator& other)	{ this->m_ptr = other.base_ptr(); this->_next = other.next_ptr(); this->_previous = other.previous_ptr(); return (*this); };

			template <typename U>
			const bd_iterator & operator= (const bd_iterator<U, false>& other)	{ this->m_ptr = other.base_ptr(); this->_next = other.next_ptr(); this->_previous = other.previous_ptr(); return (*this); };
			bd_iterator& operator++ () {
				node_pointer	tmp = this->m_ptr;
				this->m_ptr = this->_next;
				this->_next = (this->m_ptr == NULL ? NULL: this->m_ptr->next());
				this->_previous = (this->m_ptr == NULL ? tmp: this->m_ptr->previous());
				return *this; }
			bd_iterator operator++ (int) {
				node_pointer	tmp = this->m_ptr;
				bd_iterator ret = *this;
				this->m_ptr = this->_next;
				this->_next = (this->m_ptr == NULL ? NULL: this->m_ptr->next());
				this->_previous = (this->m_ptr == NULL ? tmp: this->m_ptr->previous());
				return ret; }
			bd_iterator& operator-- () {
				node_pointer	tmp = this->m_ptr;
				this->m_ptr = this->_previous;
				this->_previous = (this->m_ptr == NULL ? NULL: this->m_ptr->previous());
				this->_next = (this->m_ptr == NULL ? tmp: this->m_ptr->next());
				return *this; }
			bd_iterator operator-- (int) {
				node_pointer	tmp = this->m_ptr;
				bd_iterator ret = *this;
				this->m_ptr = this->_previous;
				this->_previous = (this->m_ptr == NULL ? NULL: this->m_ptr->previous());
				this->_next = (this->m_ptr == NULL ? tmp: this->m_ptr->next());
				return ret; }

			friend bool operator== (const bd_iterator& a, const bd_iterator& b) { return a.m_ptr == b.m_ptr; };
			template <typename U>
			friend bool operator== (const bd_iterator& a, const bd_iterator<U, false>& b) { return a.m_ptr == b.base_ptr(); };

			friend bool operator!= (const bd_iterator& a, const bd_iterator& b) { return a.m_ptr != b.m_ptr; };
			template <typename U>
			friend bool operator!= (const bd_iterator& a, const bd_iterator<U, false>& b) { return a.m_ptr != b.base_ptr(); };
	};

	template <typename T, bool constness = false>
	struct	ra_iterator	: public ft::random_access_iterator<typename ft::is_const<T, constness>::type >	{
		typedef	typename ft::random_access_iterator<T>	iterator_type;
		typedef typename ft::iterator_traits<iterator_type>::difference_type 		difference_type;
		typedef typename ft::iterator_traits<iterator_type>::value_type				value_type;
		typedef typename ft::iterator_traits<iterator_type>::pointer				pointer;
		typedef typename ft::iterator_traits<iterator_type>::reference				reference;
		typedef typename ft::iterator_traits<iterator_type>::iterator_category		iterator_category;

		protected:
			pointer	m_ptr;

		public:
			ra_iterator()	:	m_ptr(NULL) {};
			ra_iterator(pointer ptr)	:	m_ptr(ptr) {};
			ra_iterator(const ra_iterator& other)	:	m_ptr(&(*other)) {};

			template<typename U>
			ra_iterator(const ra_iterator<U, false>& other)	:	m_ptr(&(*other)) {};
			virtual ~ra_iterator()	{};
			reference operator*() const { return *this->m_ptr; }
			pointer operator->() { return this->m_ptr; }

			const ra_iterator & operator= (const ra_iterator & other)	{ this->m_ptr = &(*other); return (*this); };
			template<typename U>
			const ra_iterator & operator= (const ra_iterator<U, false>& other)	{ this->m_ptr = &(*other); return (*this); };
			ra_iterator& operator++ () { ++this->m_ptr; return *this; }  
			ra_iterator operator++ (int) { ra_iterator tmp = *this; this->m_ptr++; return tmp; }
			ra_iterator& operator--() { --this->m_ptr; return *this; }  
			ra_iterator operator-- (int) { ra_iterator tmp = *this; this->m_ptr--; return tmp; }
			ra_iterator& operator+= (const difference_type& n) { difference_type m = n; if (m >= 0) while (m--) ++*this; else while (m++) --*this; return (*this); };
			ra_iterator& operator-= (const difference_type& n) { return(*this += -n); };

			ra_iterator operator+ (const difference_type& n) const { ra_iterator tmp = *this; return(tmp += n); };
			ra_iterator operator- (const difference_type& n) const { ra_iterator tmp = *this; return (tmp -= n); };

			reference operator[](difference_type n) const { return (*(*this + n)); };

			friend ra_iterator operator+ (const difference_type& n, const ra_iterator& a) { ra_iterator tmp = a; return (tmp += n); };
			friend ra_iterator operator- (const difference_type& n, const ra_iterator& a) { ra_iterator tmp = a; return (tmp -= n); };

			friend difference_type operator- (const ra_iterator &a, const ra_iterator& b) { return a.m_ptr - b.m_ptr; };
			
			template<typename U>
			friend difference_type operator- (const ra_iterator &a, const ra_iterator<U, false>& b) { return a.m_ptr - &(*b); };
			
			friend bool operator< (const ra_iterator& a, const ra_iterator& b) { return a.m_ptr < b.m_ptr; };
			
			template<typename U>
			friend bool operator< (const ra_iterator& a, const ra_iterator<U, false>& b) { return a.m_ptr < &(*b); };
			
			friend bool operator> (const ra_iterator& a, const ra_iterator& b) { return a.m_ptr > b.m_ptr; };
			
			template<typename U>
			friend bool operator> (const ra_iterator& a, const ra_iterator<U, false>& b) { return a.m_ptr > &(*b); };
			
			friend bool operator<= (const ra_iterator& a, const ra_iterator& b) { return a.m_ptr <= b.m_ptr; };
			
			template<typename U>
			friend bool operator<= (const ra_iterator& a, const ra_iterator<U, false>& b) { return a.m_ptr <= &(*b); };
			
			friend bool operator>= (const ra_iterator& a, const ra_iterator& b) { return a.m_ptr >= b.m_ptr; };
			
			template<typename U>
			friend bool operator>= (const ra_iterator& a, const ra_iterator<U, false>& b) { return a.m_ptr >= &(*b); };
			
			friend bool operator== (const ra_iterator& a, const ra_iterator& b) { return a.m_ptr == b.m_ptr; };
			
			template<typename U>
			friend bool operator== (const ra_iterator& a, const ra_iterator<U, false>& b) { return a.m_ptr == &(*b); };
			
			friend bool operator!= (const ra_iterator& a, const ra_iterator& b) { return a.m_ptr != b.m_ptr; };
			
			template<typename U>
			friend bool operator!= (const ra_iterator& a, const ra_iterator<U, false>& b) { return a.m_ptr != &(*b); };
	};

	template < class Iter >
	struct reverse_iterator	: public ft::iterator<typename ft::iterator_traits<Iter>::iterator_category,
											typename ft::iterator_traits<Iter>::value_type,
											typename ft::iterator_traits<Iter>::difference_type,
											typename ft::iterator_traits<Iter>::pointer,
											typename ft::iterator_traits<Iter>::reference>	{
		typedef	Iter	iterator_type;
		typedef typename iterator_type::difference_type 		difference_type;
		typedef typename iterator_type::value_type				value_type;
		typedef typename iterator_type::pointer					pointer;
		typedef typename iterator_type::reference				reference;
		typedef typename iterator_type::iterator_category		iterator_category;

		private:
			iterator_type	_it;

		public:
			reverse_iterator()	:	_it(NULL) {};
			reverse_iterator(iterator_type x)	:	_it(x) {};
			template <class U>
			reverse_iterator(const reverse_iterator<U>& other)	:	_it(other.base()) {};
			virtual ~reverse_iterator()	{};
			reference operator*() const { iterator_type tmp = this->_it; return *(--tmp); }
			pointer operator->() const { return &(operator*()); }
			iterator_type	base()	const { return (this->_it); }

			const reverse_iterator & operator= (const reverse_iterator & other)	{ this->_it = other.base(); return (*this); };
			reverse_iterator& operator++ () { --(this->_it); return *this; };
			reverse_iterator operator++ (int) { reverse_iterator tmp = *this; --(this->_it); return tmp; };
			reverse_iterator& operator--() { ++(this->_it); return *this; };
			reverse_iterator operator-- (int) { reverse_iterator tmp = *this; ++(this->_it); return tmp; };
			reverse_iterator operator+ (const difference_type& n) const { return reverse_iterator(this->base() - n); };
			reverse_iterator operator- (const difference_type& n) const { return reverse_iterator(this->base() + n); };
			reverse_iterator operator+= (const difference_type& n) { (this->_it -= n); return (*this); };
			reverse_iterator operator-= (const difference_type& n) { (this->_it += n); return (*this); };
			reference operator[](const difference_type& n) const { return (this->_it[-n -1]); };
	};

	template< class Iterator1 >
	reverse_iterator<Iterator1> operator+ (const typename reverse_iterator<Iterator1>::difference_type& n, const reverse_iterator<Iterator1>& a) { return reverse_iterator<Iterator1>(a.base() - n); };

	template< class Iterator1 >
	reverse_iterator<Iterator1> operator- (const typename reverse_iterator<Iterator1>::difference_type& n, const reverse_iterator<Iterator1>& a) { return reverse_iterator<Iterator1>(a.base() + n); };

	template< class Iterator1, class Iterator2 >
	typename reverse_iterator<Iterator1>::difference_type operator- (const reverse_iterator<Iterator1>&a, const reverse_iterator<Iterator2>& b) { return b.base() - a.base(); };

	template< class Iterator1, class Iterator2 >
	bool operator< (const reverse_iterator<Iterator1>& a, const reverse_iterator<Iterator2>& b) { return a.base() > b.base(); };

	template< class Iterator1, class Iterator2 >
	bool operator> (const reverse_iterator<Iterator1>& a, const reverse_iterator<Iterator2>& b) { return a.base() < b.base(); };

	template< class Iterator1, class Iterator2 >
	bool operator<= (const reverse_iterator<Iterator1>& a, const reverse_iterator<Iterator2>& b) { return a.base() >= b.base(); };

	template< class Iterator1, class Iterator2 >
	bool operator>= (const reverse_iterator<Iterator1>& a, const reverse_iterator<Iterator2>& b) { return a.base() <= b.base(); };

	template< class Iterator1, class Iterator2 >
	bool operator== (const reverse_iterator<Iterator1>& a, const reverse_iterator<Iterator2>& b) { return a.base() == b.base(); };

	template< class Iterator1, class Iterator2 >
	bool operator!= (const reverse_iterator<Iterator1>& a, const reverse_iterator<Iterator2>& b) { return a.base() != b.base(); };
}

#endif
