#include "pair.hpp"

namespace	ft	{
	template <class T1, class T2>
	pair<T1, T2>&	pair<T1, T2>::operator=(const pair& other)
	{
		this->first = other.first;
		this->second = other.second;
		return (*this);
	}

	template <class T1, class T2>
	pair<T1, T2>	make_pair(T1 t, T2 u)
	{
		return (pair<T1, T2>(t, u));
	}

	template < class T1, class T2 >
	bool operator==( const pair<T1, T2>& lhs,
						 const pair<T1, T2>& rhs )
	{
		if (lhs.first == rhs.first && lhs.second == rhs.second)
			return (true);
		else
			return (false);
	}

	template < class T1, class T2 >
	bool operator!=( const pair<T1, T2>& lhs,
						 const pair<T1, T2>& rhs )
	{
		return (!(lhs == rhs));
	}

	template < class T1, class T2 >
	bool operator<( const pair<T1, T2>& lhs,
						const pair<T1, T2>& rhs )
	{
		if (lhs.first < rhs.first)
			return (true);
		else if (rhs.first < lhs.first)
			return (false);
		else if (lhs.second < rhs.second)
			return (true);
		else
			return (false);
	}

	template < class T1, class T2 >
	bool operator<=( const pair<T1, T2>& lhs,
						 const pair<T1, T2>& rhs )
	{
		return (!(lhs > rhs));
	}

	template < class T1, class T2 >
	bool operator>( const pair<T1, T2>& lhs,
						const pair<T1, T2>& rhs )
	{
		if (lhs.first > rhs.first)
			return (true);
		else if (rhs.first > lhs.first)
			return (false);
		else if (lhs.second > rhs.second)
			return (true);
		else
			return (false);
	}

	template < class T1, class T2 >
	bool operator>=( const pair<T1, T2>& lhs,
						 const pair<T1, T2>& rhs )
	{
		return (!(lhs < rhs));
	}
}
