#include "rb_tree.hpp"
#include <cmath>

namespace	ft	{
	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	rb_tree<Key, Mapped, Comp_Fn, Alloc>::rb_tree()	{
		this->_head = NULL;
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	rb_tree<Key, Mapped, Comp_Fn, Alloc>::rb_tree(node_type *head)	{
		this->_head = head;
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	rb_tree<Key, Mapped, Comp_Fn, Alloc>::rb_tree(const Comp_Fn&  comp_fn, const Alloc& alloc)	{
		this->_head = NULL;
		this->comp = comp_fn;
		this->_alloc = alloc;
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	rb_tree<Key, Mapped, Comp_Fn, Alloc>::~rb_tree()	{
		this->rec_clean(this->_head);
		this->_head = NULL;
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::rec_clean(node_pointer ptr)	{
		if (ptr == NULL)
			return ;
		if (ptr->getLeft() != NULL)
			this->rec_clean(ptr->getLeft());
		if (ptr->getRight() != NULL)
			this->rec_clean(ptr->getRight());
		if (ptr->getLeft() == NULL && ptr->getRight() == NULL)
		{
			if (ptr->getParent() != NULL)
			{
				if (ptr->getParent()->getLeft() == ptr)
					ptr->getParent()->setLeft(NULL);
				else
					ptr->getParent()->setRight(NULL);
			}
			delete	ptr;
			return ;
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	rb_tree<Key, Mapped, Comp_Fn, Alloc> & rb_tree<Key, Mapped, Comp_Fn, Alloc>::operator=(rb_tree &ref)	{
		(void)ref;
		return (*this);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::push(const value_type& ref)	{
		node_pointer	ptr = this->_head;
		node_pointer	tmp = this->_alloc.allocate(1);

		this->_alloc.construct(tmp, ref);
		while (ptr != NULL)
		{
			if (comp(tmp->getContent().first, ptr->getContent().first))
			{
				if (ptr->getLeft() == NULL)
				{
					tmp->setParent(ptr);
					ptr->setLeft(tmp);
					tmp->balance(&this->_head);
					return (tmp);
				}
				ptr = ptr->getLeft();
			}
			else if (comp(ptr->getContent().first, tmp->getContent().first))
			{
				if (ptr->getRight() == NULL)
				{
					tmp->setParent(ptr);
					ptr->setRight(tmp);
					tmp->balance(&this->_head);
					return (tmp);
				}
				ptr = ptr->getRight();
			}
			else
			{
				this->_alloc.destroy(tmp);
				this->_alloc.deallocate(tmp, 1);
				return (ptr);
			}
		}
		this->_head = tmp;
		return (tmp);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::erase(const key_type& key)	{
		node_pointer	ptr = this->_head;

		while (ptr != NULL)
		{
			if (comp(key, ptr->getContent().first))
			{
				if (ptr->getLeft() == NULL)
					return (0);
				ptr = ptr->getLeft();
			}
			else if (comp(ptr->getContent().first, key))
			{
				if (ptr->getRight() == NULL)
					return (0);
				ptr = ptr->getRight();
			}
			else
				return (delete_node(ptr));
		}
		return (0);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::delete_node(node_pointer ptr)	{
		if (ptr->getParent() == NULL)
		{
			if (ptr->getRight() == NULL)
			{
				if (ptr->getLeft() == NULL)
					this->_head = NULL;
				else
				{
					this->_head = ptr->getLeft();
					this->_head->setParent(NULL);
				}
			}
			else if (ptr->getLeft() == NULL)
			{
				this->_head = ptr->getRight();
				this->_head->setParent(NULL);
			}
			else
			{
				if (ptr->getLeft()->maxDepth(1) >= ptr->getRight()->maxDepth(1))
					deletion_swap_predecessor_head(ptr);
				else
					deletion_swap_successor_head(ptr);
			}
		}
		else
		{
			if (ptr->getParent()->getRight() == ptr)
				this->delete_right_child(ptr);
			else
				this->delete_left_child(ptr);
		}

		if (ptr->getParent() != NULL)
		{
			if (ptr->getParent()->getRight() == ptr)
			{
				if (ptr->getLeft() == NULL)
				{
					ptr->getParent()->setRight(ptr->getRight());
					if (ptr->getRight() != NULL)
						ptr->getRight()->setParent(ptr->getParent());
				}
				else if (ptr->getRight() == NULL)
				{
					ptr->getParent()->setRight(ptr->getLeft());
					ptr->getLeft()->setParent(ptr->getParent());
				}
				else
					ptr->getParent()->setRight(NULL);
			}
			else if (ptr->getParent()->getLeft() == ptr)
			{
				if (ptr->getLeft() == NULL)
				{
					ptr->getParent()->setLeft(ptr->getRight());
					if (ptr->getRight() != NULL)
						ptr->getRight()->setParent(ptr->getParent());
				}
				else if (ptr->getRight() == NULL)
				{
					ptr->getParent()->setLeft(ptr->getLeft());
					ptr->getLeft()->setParent(ptr->getParent());
				}
				else
					ptr->getParent()->setLeft(NULL);
			}
		}
		this->_alloc.destroy(ptr);
		this->_alloc.deallocate(ptr, 1);
		return (1);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::delete_right_child(node_pointer ptr)	{
		if (ptr->getRight() == NULL)
		{
			if (ptr->getLeft() == NULL && ptr->getColor() == RED)
				ptr->getParent()->setRight(NULL);
			else if (ptr->getLeft() != NULL && ptr->getLeft()->getColor() == RED)
			{
				ptr->getLeft()->recolor(BLACK);
				ptr->getLeft()->setParent(ptr->getParent());
				ptr->getParent()->setRight(ptr->getLeft());
			}
			else
				this->black_node_deletion(ptr);
		}
		else if (ptr->getLeft() == NULL)
		{
			if (ptr->getRight()->getColor() == RED)
			{
				ptr->getRight()->recolor(BLACK);
				ptr->getRight()->setParent(ptr->getParent());
				ptr->getParent()->setRight(ptr->getRight());
			}
			else
				this->black_node_deletion(ptr);
		}
		else
		{
			if (ptr->getLeft()->maxDepth(1) >= ptr->getRight()->maxDepth(1))
				deletion_swap_predecessor_right(ptr);
			else
				deletion_swap_successor_right(ptr);
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::delete_left_child(node_pointer ptr)	{
		if (ptr->getRight() == NULL)
		{
			if (ptr->getLeft() == NULL && ptr->getColor() == RED)
				ptr->getParent()->setLeft(NULL);
			else if (ptr->getLeft() != NULL && ptr->getLeft()->getColor() == RED)
			{
				ptr->getLeft()->recolor(BLACK);
				ptr->getLeft()->setParent(ptr->getParent());
				ptr->getParent()->setLeft(ptr->getLeft());
			}
			else
				this->black_node_deletion(ptr);
		}
		else if (ptr->getLeft() == NULL)
		{
			if (ptr->getRight()->getColor() == RED)
			{
				ptr->getRight()->recolor(BLACK);
				ptr->getRight()->setParent(ptr->getParent());
				ptr->getParent()->setLeft(ptr->getRight());
			}
			else
				this->black_node_deletion(ptr);
		}
		else
		{
			if (ptr->getLeft()->maxDepth(1) >= ptr->getRight()->maxDepth(1))
				deletion_swap_predecessor_left(ptr);
			else
				deletion_swap_successor_left(ptr);
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::deletion_swap_predecessor_head(node_pointer ptr)	{
		node_pointer	tmp = ptr->getPredecessor();

		if (tmp == ptr->getLeft())
		{
			tmp->setParent(NULL);
			this->_head = tmp;
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->recolor(ptr->getColor());
			if (tmp->getColor() == RED || (tmp->getLeft() != NULL &&  tmp->getLeft()->getColor() == RED))
			{
				if (tmp->getLeft() != NULL)
					tmp->getLeft()->recolor(BLACK);
			}
		}
		else
		{
			this->_head = tmp;
			if (tmp->getLeft() != NULL)
			{
				if (tmp->getColor() == RED || tmp->getLeft()->getColor() == RED)
					tmp->getLeft()->recolor(BLACK);
				tmp->getParent()->setRight(tmp->getLeft());
				tmp->getLeft()->setParent(tmp->getParent());
			}
			else
				tmp->getParent()->setRight(NULL);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->setParent(NULL);
			tmp->recolor(ptr->getColor());
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::deletion_swap_successor_head(node_pointer ptr)	{
		node_pointer	tmp = ptr->getSuccessor();

		if (tmp == ptr->getRight())
		{
			tmp->setParent(NULL);
			this->_head = tmp;
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->recolor(ptr->getColor());
			if (tmp->getColor() == RED ||(tmp->getRight() != NULL &&  tmp->getRight()->getColor() == RED))
			{
				if (tmp->getRight() != NULL)
					tmp->getRight()->recolor(BLACK);
			}
		}
		else
		{
			this->_head = tmp;
			if (tmp->getRight() != NULL)
			{
				if (tmp->getColor() == RED || tmp->getRight()->getColor() == RED)
					tmp->getRight()->recolor(BLACK);
				tmp->getParent()->setLeft(tmp->getRight());
				tmp->getRight()->setParent(tmp->getParent());
			}
			else
				tmp->getParent()->setLeft(NULL);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->setParent(NULL);
			tmp->recolor(ptr->getColor());
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::deletion_swap_predecessor_right(node_pointer ptr)	{
		node_pointer	tmp = ptr->getPredecessor();

		if (tmp == ptr->getLeft())
		{
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setRight(tmp);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->recolor(ptr->getColor());
			if (tmp->getColor() == RED || (tmp->getLeft() != NULL &&  tmp->getLeft()->getColor() == RED))
			{
				if (tmp->getLeft() != NULL)
					tmp->getLeft()->recolor(BLACK);
			}
		}
		else
		{
			if (tmp->getLeft() != NULL)
			{
				if (tmp->getColor() == RED || tmp->getLeft()->getColor() == RED)
						tmp->getLeft()->recolor(BLACK);
				tmp->getParent()->setRight(tmp->getLeft());
				tmp->getLeft()->setParent(tmp->getParent());
			}
			else
				tmp->getParent()->setRight(NULL);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setRight(tmp);
			tmp->recolor(ptr->getColor());
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::deletion_swap_successor_right(node_pointer ptr)	{
		node_pointer	tmp = ptr->getSuccessor();

		if (tmp == ptr->getRight())
		{
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setRight(tmp);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->recolor(ptr->getColor());
			if (tmp->getColor() == RED ||(tmp->getRight() != NULL &&  tmp->getRight()->getColor() == RED))
			{
				if (tmp->getRight() != NULL)
					tmp->getRight()->recolor(BLACK);
			}
		}
		else
		{
			if (tmp->getRight() != NULL)
			{
				if (tmp->getColor() == RED || tmp->getRight()->getColor() == RED)
					tmp->getRight()->recolor(BLACK);
				tmp->getParent()->setLeft(tmp->getRight());
				tmp->getRight()->setParent(tmp->getParent());
			}
			else
				tmp->getParent()->setLeft(NULL);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setRight(tmp);
			tmp->recolor(ptr->getColor());
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::deletion_swap_predecessor_left(node_pointer ptr)	{
		node_pointer	tmp = ptr->getPredecessor();

		if (tmp == ptr->getLeft())
		{
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setLeft(tmp);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->recolor(ptr->getColor());
			if (tmp->getColor() == RED || (tmp->getLeft() != NULL &&  tmp->getLeft()->getColor() == RED))
			{
				if (tmp->getLeft() != NULL)
					tmp->getLeft()->recolor(BLACK);
			}
		}
		else
		{
			if (tmp->getLeft() != NULL)
			{
				if (tmp->getColor() == RED || tmp->getLeft()->getColor() == RED)
					tmp->getLeft()->recolor(BLACK);
				tmp->getParent()->setRight(tmp->getLeft());
				tmp->getLeft()->setParent(tmp->getParent());
			}
			else
				tmp->getParent()->setRight(NULL);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setLeft(tmp);
			tmp->recolor(ptr->getColor());
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::deletion_swap_successor_left(node_pointer ptr)	{
		node_pointer	tmp = ptr->getSuccessor();

		if (tmp == ptr->getRight())
		{
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setLeft(tmp);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->recolor(ptr->getColor());
			if (tmp->getColor() == RED || (tmp->getRight() != NULL &&  tmp->getRight()->getColor() == RED))
			{
				if (tmp->getLeft() != NULL)
					tmp->getRight()->recolor(BLACK);
			}
		}
		else
		{
			if (tmp->getRight() != NULL)
			{
				if (tmp->getColor() == RED || tmp->getRight()->getColor() == RED)
					tmp->getRight()->recolor(BLACK);
				tmp->getParent()->setLeft(tmp->getRight());
				tmp->getRight()->setParent(tmp->getParent());
			}
			else
				tmp->getParent()->setLeft(NULL);
			tmp->setLeft(ptr->getLeft());
			if (ptr->getLeft() != NULL)
				ptr->getLeft()->setParent(tmp);
			tmp->setRight(ptr->getRight());
			if (ptr->getRight() != NULL)
				ptr->getRight()->setParent(tmp);
			tmp->setParent(ptr->getParent());
			ptr->getParent()->setLeft(tmp);
			tmp->recolor(ptr->getColor());
		}
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::black_node_deletion(node_pointer ptr)	{
		if (ptr->getParent() == NULL)
			return (1);
		if (ptr->getSibling() != NULL && ptr->getSibling()->getColor() == RED)
			return (black_node_deletion2(ptr));
		if (ptr->getDistantNephew() != NULL && ptr->getDistantNephew()->getColor() == RED)
			return (black_node_deletion5(ptr));
		if (ptr->getCloseNephew() != NULL && ptr->getCloseNephew()->getColor() == RED)
			return (black_node_deletion4(ptr));
		if (ptr->getParent()->getColor() == RED)
			return (black_node_deletion3(ptr));
		if (ptr->getSibling() != NULL)
			ptr->getSibling()->recolor(RED);
		return (this->black_node_deletion(ptr->getParent()));
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::black_node_deletion2(node_pointer ptr)	{
		ptr->getParent()->recolor(RED);
		ptr->getSibling()->recolor(BLACK);
		if (ptr->getParent()->getRight() == ptr)
			ptr->getParent()->rotate_right(&this->_head);
		else
			ptr->getParent()->rotate_left(&this->_head);
		if (ptr->getDistantNephew() != NULL && ptr->getDistantNephew()->getColor() == RED)
			return (black_node_deletion5(ptr));
		if (ptr->getCloseNephew() != NULL && ptr->getCloseNephew()->getColor() == RED)
			return (black_node_deletion4(ptr));
		return (black_node_deletion3(ptr));
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::black_node_deletion3(node_pointer ptr)	{
		if (ptr->getSibling() != NULL)
			ptr->getSibling()->recolor(RED);
		if (ptr->getParent() != NULL)
			ptr->getParent()->recolor(BLACK);
		return (0);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::black_node_deletion4(node_pointer ptr)	{
		ptr->getSibling()->recolor(RED);
		ptr->getCloseNephew()->recolor(BLACK);
		if (ptr->getParent()->getLeft() == ptr)
			ptr->getSibling()->rotate_right(&this->_head);
		else
			ptr->getSibling()->rotate_left(&this->_head);
		return (black_node_deletion5(ptr));
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::black_node_deletion5(node_pointer ptr)	{
		ptr->getSibling()->recolor(ptr->getParent()->getColor());
		ptr->getParent()->recolor(BLACK);
		ptr->getDistantNephew()->recolor(BLACK);
		if (ptr->getParent()->getRight() == ptr)
			ptr->getParent()->rotate_right(&this->_head);
		else if (ptr->getParent()->getLeft() == ptr)
			ptr->getParent()->rotate_left(&this->_head);
		return (0);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::depth()	{
		return (this->_head->maxDepth(0));
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::blackDepth()	{
		return (this->_head->blackDepth(0));
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::print()	const {
		typename ft::vector<node_pointer>	currentNodes(1, this->_head);
		size_type	maxDepth = 0;

		if (this->_head == NULL)
		{
			std::cout << "(NULL)" << std::endl;
			return ;
		}
		maxDepth = this->_head->maxDepth(1);
		printNodes(currentNodes, 1, maxDepth);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::printNodes(typename ft::vector<node_pointer> currentNodes, size_type depth, size_type maxDepth)	const {
		typename ft::vector<node_pointer>			nextNodes;
		int								floor;
		int								firstSpace;
		int								beSpace;
		typename ft::vector<node_pointer>::iterator	ite = currentNodes.begin();

		while (ite != currentNodes.end() && *ite == NULL)
			ite++;
		if (ite == currentNodes.end())
			return;
		for (typename ft::vector<node_pointer>::iterator	it = currentNodes.begin();
				it != currentNodes.end(); it++)
		{
			if (*it == NULL)
			{
				nextNodes.push_back(NULL);
				nextNodes.push_back(NULL);
			}
			else
			{	
				if ((*it)->getLeft() == NULL)
					nextNodes.push_back(NULL);
				else
					nextNodes.push_back((*it)->getLeft());
				if ((*it)->getRight() == NULL)
					nextNodes.push_back(NULL);
				else
					nextNodes.push_back((*it)->getRight());
			}
		}
		floor = maxDepth - depth;
		firstSpace = (int) (std::pow(2, (floor)) - 1) * DIGITS;
		beSpace = (int) (std::pow(2, (floor + 1)) - 1) * DIGITS;

		for (int i = 0; i < firstSpace; i++)
			std::cout << " ";
		for (typename ft::vector<node_pointer>::iterator	it = currentNodes.begin();
				it != currentNodes.end(); it++)
		{
			if (*it == NULL)
			{
				for (int i = 0; i < DIGITS; i++)
					std::cout << " ";
			}
			else
			{
				/*
				if (DIGITS > 7)
				{
					for (int i = 0; i < (DIGITS - 5) / 2; i++)
						std::cout << " ";
				}
				*/
				if ((*it)->getColor() == BLACK)
					std::cout << "\e[33m" << (**it) << "\e[0m";
				else
					std::cout << "\e[31m" << (**it) << "\e[0m";
				/*
				if (DIGITS > 7)
				{
					for (int i = 0; i < (DIGITS - 5) / 2; i++)
						std::cout << " ";
				}
				*/
			}
			if (it + 1 != currentNodes.end())
			{
				for (int i = 0; i < beSpace; i++)
					std::cout << " ";
			}
		}
		std::cout << std::endl;
		for (int i = 0; i < firstSpace; i++)
			std::cout << " ";
		for (typename ft::vector<node_pointer>::iterator	it = currentNodes.begin();
				it != currentNodes.end(); it++)
		{
			if (*it == NULL)
			{
				for (int i = 0; i < DIGITS; i++)
					std::cout << " ";
				for (int i = 0; i < beSpace; i++)
					std::cout << " ";
			}
			else
			{
				if ((*it)->getLeft() == NULL && (*it)->getRight() == NULL)
				{
					for (int i = 0; i < DIGITS; i++)
						std::cout << " ";
					if (it + 1 != currentNodes.end())
					{
						for (int i = 0; i < beSpace; i++)
							std::cout << " ";
					}
				}
				else
				{
					for (int i = 0; i < DIGITS / 2; i++)
						std::cout << " ";
					if ((*it)->getLeft() != NULL)
					{
						for (int i = 0; i < ((beSpace * (0.25 * DIGITS)) / DIGITS); i++)
							std::cout << "\b";
						std::cout << "|";
						for (int i = 0; i < ((beSpace * (0.25 * DIGITS)) / DIGITS) - 1; i++)
							std::cout << "-";
					}
					std::cout << "|";
					if ((*it)->getRight() == NULL)
					{
						for (int i = 0; i < DIGITS / 2; i++)
							std::cout << " ";
						for (int i = 0; i < beSpace; i++)
							std::cout << " ";
					}
					else
					{
						if (beSpace % DIGITS == 0)
						{
							for (int i = 0; i < ((beSpace * (0.25 * DIGITS)) / DIGITS) + 1; i++)
								std::cout << "-";
							std::cout << "\b|";
							if (it + 1 != currentNodes.end())
							{
								for (int i = 0; i < beSpace - (((beSpace * (0.25 * DIGITS)) / DIGITS)) ; i++)
									std::cout << " ";
							}
						}
						else
						{
							for (int i = 0; i < ((beSpace * (0.25 * DIGITS)) / DIGITS); i++)
								std::cout << "-";
							std::cout << "\b|";
							if (it + 1 != currentNodes.end())
							{
								for (int i = 0; i < beSpace - (((beSpace * (0.25 * DIGITS)) / DIGITS)); i++)
									std::cout << " ";
							}
						}
					}
				}
			}
		}
		std::cout << std::endl;
		printNodes(nextNodes, depth + 1, maxDepth);
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	std::ostream & operator<<(std::ostream & o, rb_tree<Key, Mapped, Comp_Fn, Alloc> &ref)	{
	/*	!!!	CHANGE RETURN VALUE OF PRINT TO STRING OR OSSTREAM	!!! */
		ref.print();
		return (o);
	}

/***************************/
/* Accessors for map class */
/***************************/
	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::value_type&	rb_tree<Key, Mapped, Comp_Fn, Alloc>::at(const Key& key)	{
		node_pointer ret = this->find(key);

		if (ret == NULL)
			throw (std::out_of_range("map::at"));
		return (ret->getContent());
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	const typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::value_type&	rb_tree<Key, Mapped, Comp_Fn, Alloc>::at(const Key& key) const	{
		node_pointer ret = this->find(key);

		if (ret == NULL)
			throw (std::out_of_range("map::at"));
		return (ret->getContent());
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::value_type&	rb_tree<Key, Mapped, Comp_Fn, Alloc>::brackets(const Key& key)	{
		node_pointer ret = this->find(key);

		if (ret == NULL)
			ret = this->push(ft::make_pair(key, Mapped()));
		return (ret->getContent());
	}

/***************************/
/* Iterators for map class */
/***************************/
	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::begin(){
		if (this->_head == NULL)
			return (NULL);
		return (node_pointer(this->_head->first()));
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::begin()	const	{
		if (this->_head == NULL)
			return (NULL);
		return (node_pointer(this->_head->first()));
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::end()	{
		if (this->_head == NULL)
			return (NULL);
		return (node_pointer(this->_head->last()));
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::end()	const	{
		if (this->_head == NULL)
			return (NULL);
		return (node_pointer(this->_head->last()));
	}

/**********************************/
/* Capacity methods for map class */
/**********************************/
	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	bool	rb_tree<Key, Mapped, Comp_Fn, Alloc>::empty()	const	{
		return (this->_head == NULL);
	}

/***********************************/
/* Modifiers methods for map class */
/***********************************/
	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::clear()	{
		this->rec_clean(this->_head);
		this->_head = NULL;
	}

	template < typename Key, typename Mapped, typename Comp_Fn, typename Alloc >
	ft::pair<typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer, bool>	rb_tree<Key, Mapped, Comp_Fn, Alloc>::insert(const value_type& ref)	{
		node_pointer	ptr = this->_head;
		node_pointer	tmp = this->_alloc.allocate(1);

		this->_alloc.construct(tmp, ref);
		while (ptr != NULL)
		{
			if (comp(tmp->getContent().first, ptr->getContent().first))
			{
				if (ptr->getLeft() == NULL)
				{
					tmp->setParent(ptr);
					ptr->setLeft(tmp);
					tmp->balance(&this->_head);
					return (ft::make_pair(tmp, true));
				}
				ptr = ptr->getLeft();
			}
			else if (comp(ptr->getContent().first, tmp->getContent().first))
			{
				if (ptr->getRight() == NULL)
				{
					tmp->setParent(ptr);
					ptr->setRight(tmp);
					tmp->balance(&this->_head);
					return (ft::make_pair(tmp, true));
				}
				ptr = ptr->getRight();
			}
			else
			{
				this->_alloc.destroy(tmp);
				this->_alloc.deallocate(tmp, 1);
				return (ft::make_pair(ptr, false));
			}
		}
		this->_head = tmp;
		return (ft::make_pair(tmp, true));
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::insert(node_pointer hint, const value_type& value)	{
		node_pointer	ptr = hint;
		node_pointer	tmp = this->_alloc.allocate(1);

		this->_alloc.construct(tmp, value);
		if (ptr == NULL)
		{
			delete tmp;
			return (this->push(value));
		}
		if (comp(ptr->getContent().first, tmp->getContent().first))
		{
			if (ptr->getRight() == NULL && ptr->next() != NULL && comp(tmp->getContent().first, ptr->next()->getContent().first))
			{
				ptr->setRight(tmp);
				tmp->setParent(ptr);
				return (tmp);
			}
		}
		this->_alloc.destroy(tmp);
		this->_alloc.deallocate(tmp, 1);
		return (this->push(value));
		return (NULL);
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	void	rb_tree<Key, Mapped, Comp_Fn, Alloc>::swap(rb_tree& other)	{
		node_pointer	tmp = this->_head;
		key_compare		tmp_comp = this->comp;

	//	if (this->_alloc == other._alloc)
	//	{
			this->_head = other._head;
			this->comp = other.comp;
			other._head = tmp;
			other.comp = tmp_comp;
	//	}
	}

/********************************/
/* Lookup methods for map class */
/********************************/
	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::size_type	rb_tree<Key, Mapped, Comp_Fn, Alloc>::count(const Key& key)	const	{
		if (this->find(key) != NULL)
			return (1);
		return (0);
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::find(const Key& key)	const	{
		node_pointer	ptr = this->_head;

		while (ptr != NULL)
		{
			if (comp(ptr->getContent().first, key))
				ptr = ptr->getRight();
			else if (comp(key, ptr->getContent().first))
				ptr = ptr->getLeft();
			else
				return (ptr);
		}
		return (ptr);
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	ft::pair<typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer, typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer>	rb_tree<Key, Mapped, Comp_Fn, Alloc>::equal_range(const Key& key)	const	{
		ft::pair<node_pointer, node_pointer>	tmp;

		tmp.first = lower_bound(key);
		tmp.second = upper_bound(key);
		return (tmp);
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::lower_bound(const Key& key)	const	{
		node_pointer	ptr = this->begin();

		while (ptr != NULL)
		{
			if (comp(ptr->getContent().first, key))
			{
				if (ptr->next() == NULL)
					return (NULL);
				ptr = ptr->next();
			}
			else
				return (ptr);
		}
		return (ptr);
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::node_pointer	rb_tree<Key, Mapped, Comp_Fn, Alloc>::upper_bound(const Key& key)	const	{
		node_pointer	ptr = this->begin();

		while (ptr != NULL)
		{
			if (comp(ptr->getContent().first, key))
			{
				if (ptr->next() == NULL)
					return (NULL);
				ptr = ptr->next();
			}
			else if (comp(key, ptr->getContent().first))
				return (ptr);
			else
				return (ptr->next());
		}
		return (ptr);
	}

/***************************/
/* Observers for map class */
/***************************/
	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::key_compare	rb_tree<Key, Mapped, Comp_Fn, Alloc>::key_comp()	const	{
		return (this->comp);
	}

	template <typename Key, typename Mapped, typename Comp_Fn, typename Alloc>
	typename rb_tree<Key, Mapped, Comp_Fn, Alloc>::key_compare	rb_tree<Key, Mapped, Comp_Fn, Alloc>::value_comp()	const	{
		return (this->comp);
	}
}
