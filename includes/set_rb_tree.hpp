#ifndef SET_RB_TREE_HPP
# define SET_RB_TREE_HPP

# include "node.hpp"
# include "iterators.hpp"
# include "pair.hpp"
# include "../vector/vector.hpp"

# define DIGITS	7
//# define DIGITS	5

namespace	ft	{
	template < typename Key, typename Comp_Fn, typename Alloc >
	struct	set_rb_tree	{
			typedef	Key										key_type;
			typedef Key				value_type;
			typedef	const value_type						const_value_type;
			typedef ft::node<value_type, Alloc>				node_type;
			typedef	ft::node<const_value_type, Alloc>		const_node_type;
			typedef	std::size_t								size_type;
			typedef	std::ptrdiff_t							difference_type;
			typedef	Comp_Fn									key_compare;
			typedef	typename Alloc::template rebind<node_type>::other		allocator_type;
			typedef	value_type&								reference;
			typedef	const value_type&						const_reference;
			typedef typename Alloc::pointer					pointer;
			typedef	typename Alloc::const_pointer			const_pointer;
			typedef node_type&								node_reference;
			typedef const_node_type&						const_node_reference;
			typedef node_type*								node_pointer;
			typedef const_node_type*						const_node_pointer;

			node_type *_head;
			allocator_type	_alloc;
			key_compare		comp;

			set_rb_tree();
			set_rb_tree(set_rb_tree &ref);
			explicit	set_rb_tree(const Comp_Fn& comp_fn, const Alloc& alloc);
			set_rb_tree(node_type *head);
			virtual	~set_rb_tree();
			set_rb_tree&	operator=(set_rb_tree &ref);

			void		rec_clean(node_pointer ptr);
			node_pointer			push(const value_type& ref);
			size_type	erase(const key_type& key);
			size_type	delete_node(node_pointer ptr);
			void		delete_right_child(node_pointer ptr);
			void		delete_left_child(node_pointer ptr);
			void		deletion_swap_predecessor_head(node_pointer ptr);
			void		deletion_swap_successor_head(node_pointer ptr);
			void		deletion_swap_predecessor_right(node_pointer ptr);
			void		deletion_swap_successor_right(node_pointer ptr);
			void		deletion_swap_predecessor_left(node_pointer ptr);
			void		deletion_swap_successor_left(node_pointer ptr);
			size_type	black_node_deletion(node_pointer ptr);
			size_type	black_node_deletion2(node_pointer ptr);
			size_type	black_node_deletion3(node_pointer ptr);
			size_type	black_node_deletion4(node_pointer ptr);
			size_type	black_node_deletion5(node_pointer ptr);
			size_type	depth();
			size_type	blackDepth();
			void		print() const;
			void		printNodes(ft::vector<node_pointer> currentNodes, size_type depth, size_type maxDepth) const;

		/***************************/
		/* Iterators for map class */
		/***************************/
			node_pointer	begin();
			node_pointer	begin()	const;
			node_pointer	end();
			node_pointer	end()	const;

		/**********************************/
		/* Capacity methods for map class */
		/**********************************/
			bool	empty()	const;

		/***********************************/
		/* Modifiers methods for map class */
		/***********************************/
			void	clear();

			ft::pair<node_pointer, bool>	insert(const value_type& value);
			node_pointer	insert(node_pointer hint, const value_type& value);

			void	swap(set_rb_tree& other);

		/********************************/
		/* Lookup methods for map class */
		/********************************/
			size_type	count(const Key& key)	const;
			node_pointer	find(const Key& key) const;
			ft::pair<node_pointer, node_pointer>	equal_range(const Key& key) const;
			node_pointer	lower_bound(const Key& key) const;
			node_pointer	upper_bound(const Key& key) const;

		/***************************/
		/* Observers for map class */
		/***************************/
			key_compare	key_comp()	const;
			key_compare	value_comp()	const;
	};

	template < typename Key, typename Comp_Fn, typename Alloc >
	std::ostream&	operator<<(std::ostream & o, set_rb_tree<Key, Comp_Fn, Alloc> &ref);
}

#include "set_rb_tree.cpp"
#endif
