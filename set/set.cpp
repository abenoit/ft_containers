#include "set.hpp"

namespace ft	{
/******************************/
/* Constructors for set class */
/******************************/
	template <class Key, class Compare, class Allocator>
	set<Key, Compare, Allocator>::set() {
	}

	template <class Key, class Compare, class Allocator>
	set<Key, Compare, Allocator>::set(const Compare& comp, const Allocator& alloc)	:	_M_t(comp, alloc) {
	}

	template <class Key, class Compare, class Allocator>
	template<class InputIt>
	set<Key, Compare, Allocator>::set(InputIt first, InputIt last,
		const Compare& comp,
		const Allocator& alloc)	:	_M_t(comp, alloc) {
		for (InputIt it = first; it != last; it++)
			this->insert(*it);
	}

	template <class Key, class Compare, class Allocator>
	set<Key, Compare, Allocator>::set(const set& other)	{
		for (set::const_iterator it = other.begin(); it != other.end(); it++)
			this->insert(*it);
	}

	template <class Key, class Compare, class Allocator>
	set<Key, Compare, Allocator>::~set() {
	}

	template <class Key, class Compare, class Allocator>
	set<Key, Compare, Allocator>&	set<Key, Compare, Allocator>::operator= (const set& other)	{
		this->clear();
		for (set::const_iterator it = other.begin(); it != other.end(); it++)
			this->insert(*it);
		return (*this);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::allocator_type	set<Key, Compare, Allocator>::get_allocator()	const	{
		return (_M_t._alloc);
	}

/***************************/
/* Iterators for set class */
/***************************/
	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::iterator	set<Key, Compare, Allocator>::begin()	{
		return (iterator(_M_t.begin()));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_iterator	set<Key, Compare, Allocator>::begin()	const	{
		return (const_iterator(_M_t.begin()));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::iterator	set<Key, Compare, Allocator>::end()	{
		iterator	it(_M_t.end());

		it++;
		return (it);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_iterator	set<Key, Compare, Allocator>::end()	const	{
		const_iterator	it(_M_t.end());

		it++;
		return (it);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::reverse_iterator	set<Key, Compare, Allocator>::rbegin()	{
		iterator	it(_M_t.end());
		reverse_iterator	ret(it);

		ret--;
		return (ret);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_reverse_iterator	set<Key, Compare, Allocator>::rbegin()	const	{
		const_iterator	it(_M_t.end());
		const_reverse_iterator	ret(it);

		ret--;
		return (ret);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::reverse_iterator	set<Key, Compare, Allocator>::rend()	{
		reverse_iterator	it(this->begin());
		return (it);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_reverse_iterator	set<Key, Compare, Allocator>::rend()	const	{
		const_reverse_iterator	it(this->begin());
		return (it);
	}

/**********************************/
/* Capacity methods for set class */
/**********************************/
	template <class Key, class Compare, class Allocator>
	bool	set<Key, Compare, Allocator>::empty()	const	{
		return (_M_t.empty());
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::size_type	set<Key, Compare, Allocator>::size()	const	{
		size_type	i = 0;

		for (const_iterator it = this->begin(); it != this->end(); it++)
			i++;
		return (i);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::size_type	set<Key, Compare, Allocator>::max_size()	const	{
		return (std::numeric_limits<difference_type>::max());
	}

/***********************************/
/* Modifiers methods for set class */
/***********************************/
	template <class Key, class Compare, class Allocator>
	void	set<Key, Compare, Allocator>::clear()	{
		return (_M_t.clear());
	}

	template <class Key, class Compare, class Allocator>
	ft::pair<typename set<Key, Compare, Allocator>::iterator, bool>	set<Key, Compare, Allocator>::insert(const value_type& value)	{
		ft::pair<node_pointer, bool> ret = this->_M_t.insert(value);

		return (ft::make_pair(iterator(ret.first), ret.second));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::iterator	set<Key, Compare, Allocator>::insert(iterator hint, const value_type& value)	{
		return (iterator(_M_t.insert(node_pointer(hint.base_ptr()), value)));
	}

	template <class Key, class Compare, class Allocator>
	template<class InputIt>
	void	set<Key, Compare, Allocator>::insert(InputIt first, InputIt last)	{
		for (InputIt it = first; it != last; it++)
			this->_M_t.insert(*it);
	}

	template <class Key, class Compare, class Allocator>
	void	set<Key, Compare, Allocator>::erase(iterator	pos)	{
		_M_t.delete_node(pos.base_ptr());
	}

	template <class Key, class Compare, class Allocator>
	void	set<Key, Compare, Allocator>::erase(iterator	first, iterator last)	{
		for (iterator it = first; it != last; it++)
		{
			this->erase(it);
		}
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::size_type	set<Key, Compare, Allocator>::erase(const key_type& key)	{
		return (_M_t.erase(key));
	}

	template <class Key, class Compare, class Allocator>
	void	set<Key, Compare, Allocator>::swap(set& other)	{
		return (this->_M_t.swap(other._M_t));
	}

/********************************/
/* Lookup methods for set class */
/********************************/
	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::size_type	set<Key, Compare, Allocator>::count(const Key& key)	const	{
		return (_M_t.count(key));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::iterator	set<Key, Compare, Allocator>::find(const Key& key)	{
		return (_M_t.find(key));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_iterator	set<Key, Compare, Allocator>::find(const Key& key)	const	{
		return (_M_t.find(key));
	}

	template <class Key, class Compare, class Allocator>
	ft::pair<typename set<Key, Compare, Allocator>::iterator, typename set<Key, Compare, Allocator>::iterator>	set<Key, Compare, Allocator>::equal_range(const Key& key)	{
		ft::pair<node_pointer, node_pointer>	tmp = this->_M_t.equal_range(key);
		ft::pair<iterator, iterator>	ret;

		if (tmp.first == NULL)
			ret.first = this->end();
		else
			ret.first = iterator(tmp.first);
		if (tmp.second == NULL)
			ret.second = this->end();
		else
			ret.second = iterator(tmp.second);
		return (ret);
	}

	template <class Key, class Compare, class Allocator>
	ft::pair<typename set<Key, Compare, Allocator>::const_iterator, typename set<Key, Compare, Allocator>::const_iterator>	set<Key, Compare, Allocator>::equal_range(const Key& key)	const	{
		ft::pair<node_pointer, node_pointer>	tmp = this->_M_t.equal_range(key);
		ft::pair<const_iterator, const_iterator>	ret;

		if (tmp.first == NULL)
			ret.first = this->end();
		else
			ret.first = const_iterator(tmp.first);
		if (tmp.second == NULL)
			ret.second = this->end();
		else
			ret.second = const_iterator(tmp.second);
		return (ret);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::iterator	set<Key, Compare, Allocator>::lower_bound(const Key& key)	{
		node_pointer	tmp = this->_M_t.lower_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (iterator(tmp));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_iterator	set<Key, Compare, Allocator>::lower_bound(const Key& key)	const	{
		node_pointer	tmp = this->_M_t.lower_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (const_iterator(tmp));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::iterator	set<Key, Compare, Allocator>::upper_bound(const Key& key)	{
		node_pointer	tmp = this->_M_t.upper_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (iterator(tmp));
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::const_iterator	set<Key, Compare, Allocator>::upper_bound(const Key& key)	const	{
		node_pointer	tmp = this->_M_t.upper_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (const_iterator(tmp));
	}

/***************************/
/* Observers for set class */
/***************************/
	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::key_compare	set<Key, Compare, Allocator>::key_comp()	const	{
		return (this->_M_t.comp);
	}

	template <class Key, class Compare, class Allocator>
	typename set<Key, Compare, Allocator>::value_compare	set<Key, Compare, Allocator>::value_comp()	const	{
		return (value_compare(this->_M_t.comp));
	}

/************************************/
/* Comparison methods for set class */
/************************************/
	template <class Key, class Compare, class Allocator>
	bool	operator== (const ft::set<Key, Compare, Allocator>& lhs,
						const ft::set<Key, Compare, Allocator>& rhs)	{
		return (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class Key, class Compare, class Allocator>
	bool	operator!= (const ft::set<Key, Compare, Allocator>& lhs,
						const ft::set<Key, Compare, Allocator>& rhs)	{
		return (!(lhs.size() == rhs.size()) || !ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class Key, class Compare, class Allocator>
	bool	operator< (const ft::set<Key, Compare, Allocator>& lhs,
						const ft::set<Key, Compare, Allocator>& rhs)	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template <class Key, class Compare, class Allocator>
	bool	operator<= (const ft::set<Key, Compare, Allocator>& lhs,
						const ft::set<Key, Compare, Allocator>& rhs)	{
		if (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()))
			return (true);
		else if (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()))
			return (true);
		else
			return (false);
	}

	template <class Key, class Compare, class Allocator>
	bool	operator> (const ft::set<Key, Compare, Allocator>& lhs,
						const ft::set<Key, Compare, Allocator>& rhs)	{
		if (!ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		  && (lhs.size() > rhs.size() || !ft::equal(lhs.begin(), lhs.end(), rhs.begin())))
			return (true);
		else
			return (false);
	}

	template <class Key, class Compare, class Allocator>
	bool	operator>= (const ft::set<Key, Compare, Allocator>& lhs,
						const ft::set<Key, Compare, Allocator>& rhs)	{
		if (!ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		  && (lhs.size() > rhs.size() || !ft::equal(lhs.begin(), lhs.end(), rhs.begin())))
			return (true);
		else if (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()))
			return (true);
		else
			return (false);
	}

	template <class Key, class Compare, class Allocator>
	void	swap(ft::set<Key, Compare, Allocator>& lhs,
					ft::set<Key, Compare, Allocator>& rhs)	{
		lhs.swap(rhs);
	}
}
