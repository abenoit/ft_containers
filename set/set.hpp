#ifndef	SET_HPP
# define	SET_HPP

# include <limits>
# include "../includes/iterators.hpp"
# include "../includes/node.hpp"
# include "../includes/set_rb_tree.hpp"
# include "../includes/pair.hpp"

namespace ft	{
	template <class Key,
				class Compare = std::less<Key>,
				class Allocator = std::allocator<Key> >
	class set	{
		public:
			typedef	Key										key_type;
			typedef	Key					value_type;
			typedef	std::size_t								size_type;
			typedef	std::ptrdiff_t							difference_type;
			typedef	Compare									key_compare;
			typedef	Allocator								allocator_type;
			typedef	value_type&								reference;
			typedef	const value_type&						const_reference;
			typedef typename Allocator::pointer				pointer;
			typedef	typename Allocator::const_pointer		const_pointer;
			typedef	ft::bd_iterator<value_type, true>				iterator;
			typedef	ft::bd_iterator<value_type, true>		const_iterator;
			typedef	ft::reverse_iterator<iterator>			reverse_iterator;
			typedef	ft::reverse_iterator<const_iterator>	const_reverse_iterator;

			class	value_compare	:	public std::binary_function<value_type, value_type, bool>
			{
				/* Either friend or non protected constructor. Befriending this subclass corresponds to implementation */
				friend class set;
				protected:
					Compare	comp;
					value_compare(Compare c) : comp(c) {};

				public:
					bool	operator()(const value_type& lhs, const value_type & rhs)	const	{ return comp(lhs, rhs); };
			};

		private:
			typedef ft::node<value_type, Allocator>					node_type;
			typedef	node_type*								node_pointer;
			typedef	set_rb_tree<key_type, Compare, Allocator>		data_type;
			data_type		_M_t;
			key_compare		_comp;

		public:
		/******************************/
		/* Constructors for set class */
		/******************************/
			set();
			explicit	set(const Compare& comp, const Allocator& alloc = Allocator());

			template<class InputIt>
			set(InputIt first, InputIt last,
				const Compare& comp = Compare(),
				const Allocator& alloc = Allocator());

			set(const set& other);

			~set();

			set&	operator= (const set& other);

			allocator_type	get_allocator()	const;

		/***************************/
		/* Iterators for set class */
		/***************************/
			iterator	begin();
			const_iterator	begin()	const;
			iterator	end();
			const_iterator	end()	const;
			reverse_iterator	rbegin();
			const_reverse_iterator	rbegin()	const;
			reverse_iterator	rend();
			const_reverse_iterator	rend()	const;

		/**********************************/
		/* Capacity methods for set class */
		/**********************************/
			bool	empty()	const;
			size_type	size()	const;
			size_type	max_size()	const;

		/***********************************/
		/* Modifiers methods for set class */
		/***********************************/
			void	clear();

			ft::pair<iterator, bool>	insert(const value_type& value);
			iterator	insert(iterator hint, const value_type& value);

			template<class InputIt>
			void	insert(InputIt first, InputIt last);

			void	erase(iterator	pos);
			void erase( iterator first, iterator last );
			size_type	erase(const	key_type& key);

			void	swap(set& other);

		/********************************/
		/* Lookup methods for set class */
		/********************************/
			size_type	count(const Key& key)	const;
			iterator	find(const Key& key);
			const_iterator	find(const Key& key)	const;
			ft::pair<iterator, iterator>	equal_range(const Key& key);
			ft::pair<const_iterator, const_iterator>	equal_range(const Key& key)	const;
			iterator	lower_bound(const Key& key);
			const_iterator	lower_bound(const Key& key)	const;
			iterator	upper_bound(const Key& key);
			const_iterator	upper_bound(const Key& key)	const;

		/***************************/
		/* Observers for set class */
		/***************************/
			key_compare	key_comp()	const;
			value_compare	value_comp()	const;
	};

	/************************************/
	/* Comparison methods for set class */
	/************************************/
	template <class Key, class Compare, class Alloc>
	bool	operator== (const ft::set<Key, Compare, Alloc>& lhs,
						const ft::set<Key, Compare, Alloc>& rhs);

	template <class Key, class Compare, class Alloc>
	bool	operator!= (const ft::set<Key, Compare, Alloc>& lhs,
						const ft::set<Key, Compare, Alloc>& rhs);

	template <class Key, class Compare, class Alloc>
	bool	operator< (const ft::set<Key, Compare, Alloc>& lhs,
						const ft::set<Key, Compare, Alloc>& rhs);

	template <class Key, class Compare, class Alloc>
	bool	operator<= (const ft::set<Key, Compare, Alloc>& lhs,
						const ft::set<Key, Compare, Alloc>& rhs);

	template <class Key, class Compare, class Alloc>
	bool	operator> (const ft::set<Key, Compare, Alloc>& lhs,
						const ft::set<Key, Compare, Alloc>& rhs);

	template <class Key, class Compare, class Alloc>
	bool	operator>= (const ft::set<Key, Compare, Alloc>& lhs,
						const ft::set<Key, Compare, Alloc>& rhs);

	template <class Key, class Compare, class Alloc>
	void	swap(const ft::set<Key, Compare, Alloc>& lhs,
					const ft::set<Key, Compare, Alloc>& rhs);
}

# include "set.cpp"
#endif
