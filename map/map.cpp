#include "map.hpp"

namespace ft	{
/******************************/
/* Constructors for map class */
/******************************/
	template <class Key, class T, class Compare, class Allocator>
	map<Key, T, Compare, Allocator>::map() {
	}

	template <class Key, class T, class Compare, class Allocator>
	map<Key, T, Compare, Allocator>::map(const Compare& comp, const Allocator& alloc)	:	_M_t(comp, alloc) {
	}

	template <class Key, class T, class Compare, class Allocator>
	template<class InputIt>
	map<Key, T, Compare, Allocator>::map(InputIt first, InputIt last,
		const Compare& comp,
		const Allocator& alloc)	:	_M_t(comp, alloc) {
		for (InputIt it = first; it != last; it++)
			this->insert(*it);
	}

	template <class Key, class T, class Compare, class Allocator>
	map<Key, T, Compare, Allocator>::map(const map& other)	{
		for (map::const_iterator it = other.begin(); it != other.end(); it++)
			this->insert(*it);
	}

	template <class Key, class T, class Compare, class Allocator>
	map<Key, T, Compare, Allocator>::~map() {
	}

	template <class Key, class T, class Compare, class Allocator>
	map<Key, T, Compare, Allocator>&	map<Key, T, Compare, Allocator>::operator= (const map& other)	{
		this->clear();
		for (map::const_iterator it = other.begin(); it != other.end(); it++)
			this->insert(*it);
		return (*this);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::allocator_type	map<Key, T, Compare, Allocator>::get_allocator()	const	{
		return (_M_t._alloc);
	}

/***************************/
/* Accessors for map class */
/***************************/
	template <class Key, class T, class Compare, class Allocator>
	T&	map<Key, T, Compare, Allocator>::at(const Key& key)	{
		return (_M_t.at(key).second);
	}

	template <class Key, class T, class Compare, class Allocator>
	const T&	map<Key, T, Compare, Allocator>::at(const Key& key) const	{
		return (_M_t.at(key).second);
	}

	template <class Key, class T, class Compare, class Allocator>
	T&	map<Key, T, Compare, Allocator>::operator[](const Key& key)	{
		return (_M_t.brackets(key).second);
	}

/***************************/
/* Iterators for map class */
/***************************/
	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::iterator	map<Key, T, Compare, Allocator>::begin()	{
		return (iterator(_M_t.begin()));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_iterator	map<Key, T, Compare, Allocator>::begin()	const	{
		return (const_iterator(_M_t.begin()));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::iterator	map<Key, T, Compare, Allocator>::end()	{
		iterator	it(_M_t.end());

		it++;
		return (it);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_iterator	map<Key, T, Compare, Allocator>::end()	const	{
		const_iterator	it(_M_t.end());

		it++;
		return (it);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::reverse_iterator	map<Key, T, Compare, Allocator>::rbegin()	{
		iterator	it(_M_t.end());
		reverse_iterator	ret(it);

		ret--;
		return (ret);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_reverse_iterator	map<Key, T, Compare, Allocator>::rbegin()	const	{
		const_iterator	it(_M_t.end());
		const_reverse_iterator	ret(it);

		ret--;
		return (ret);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::reverse_iterator	map<Key, T, Compare, Allocator>::rend()	{
		reverse_iterator	it(this->begin());
		return (it);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_reverse_iterator	map<Key, T, Compare, Allocator>::rend()	const	{
		const_reverse_iterator	it(this->begin());
		return (it);
	}

/**********************************/
/* Capacity methods for map class */
/**********************************/
	template <class Key, class T, class Compare, class Allocator>
	bool	map<Key, T, Compare, Allocator>::empty()	const	{
		return (_M_t.empty());
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::size_type	map<Key, T, Compare, Allocator>::size()	const	{
		size_type	i = 0;

		for (const_iterator it = this->begin(); it != this->end(); it++)
			i++;
		return (i);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::size_type	map<Key, T, Compare, Allocator>::max_size()	const	{
		return (std::numeric_limits<difference_type>::max());
	}

/***********************************/
/* Modifiers methods for map class */
/***********************************/
	template <class Key, class T, class Compare, class Allocator>
	void	map<Key, T, Compare, Allocator>::clear()	{
		return (_M_t.clear());
	}

	template <class Key, class T, class Compare, class Allocator>
	ft::pair<typename map<Key, T, Compare, Allocator>::iterator, bool>	map<Key, T, Compare, Allocator>::insert(const value_type& value)	{
		ft::pair<node_pointer, bool> ret = this->_M_t.insert(value);

		return (ft::make_pair(iterator(ret.first), ret.second));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::iterator	map<Key, T, Compare, Allocator>::insert(iterator hint, const value_type& value)	{
		return (iterator(_M_t.insert(node_pointer(hint.base_ptr()), value)));
	}

	template <class Key, class T, class Compare, class Allocator>
	template<class InputIt>
	void	map<Key, T, Compare, Allocator>::insert(InputIt first, InputIt last)	{
		for (InputIt it = first; it != last; it++)
			this->_M_t.insert(*it);
	}

	template <class Key, class T, class Compare, class Allocator>
	void	map<Key, T, Compare, Allocator>::erase(iterator	pos)	{
		_M_t.delete_node(pos.base_ptr());
	}

	template <class Key, class T, class Compare, class Allocator>
	void	map<Key, T, Compare, Allocator>::erase(iterator	first, iterator last)	{
		for (iterator it = first; it != last; it++)
		{
			this->erase(it);
		}
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::size_type	map<Key, T, Compare, Allocator>::erase(const key_type& key)	{
		return (_M_t.erase(key));
	}

	template <class Key, class T, class Compare, class Allocator>
	void	map<Key, T, Compare, Allocator>::swap(map& other)	{
		return (this->_M_t.swap(other._M_t));
	}

/********************************/
/* Lookup methods for map class */
/********************************/
	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::size_type	map<Key, T, Compare, Allocator>::count(const Key& key)	const	{
		return (_M_t.count(key));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::iterator	map<Key, T, Compare, Allocator>::find(const Key& key)	{
		return (_M_t.find(key));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_iterator	map<Key, T, Compare, Allocator>::find(const Key& key)	const	{
		return (_M_t.find(key));
	}

	template <class Key, class T, class Compare, class Allocator>
	ft::pair<typename map<Key, T, Compare, Allocator>::iterator, typename map<Key, T, Compare, Allocator>::iterator>	map<Key, T, Compare, Allocator>::equal_range(const Key& key)	{
		ft::pair<node_pointer, node_pointer>	tmp = this->_M_t.equal_range(key);
		ft::pair<iterator, iterator>	ret;

		if (tmp.first == NULL)
			ret.first = this->end();
		else
			ret.first = iterator(tmp.first);
		if (tmp.second == NULL)
			ret.second = this->end();
		else
			ret.second = iterator(tmp.second);
		return (ret);
	}

	template <class Key, class T, class Compare, class Allocator>
	ft::pair<typename map<Key, T, Compare, Allocator>::const_iterator, typename map<Key, T, Compare, Allocator>::const_iterator>	map<Key, T, Compare, Allocator>::equal_range(const Key& key)	const	{
		ft::pair<node_pointer, node_pointer>	tmp = this->_M_t.equal_range(key);
		ft::pair<const_iterator, const_iterator>	ret;

		if (tmp.first == NULL)
			ret.first = this->end();
		else
			ret.first = const_iterator(tmp.first);
		if (tmp.second == NULL)
			ret.second = this->end();
		else
			ret.second = const_iterator(tmp.second);
		return (ret);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::iterator	map<Key, T, Compare, Allocator>::lower_bound(const Key& key)	{
		node_pointer	tmp = this->_M_t.lower_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (iterator(tmp));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_iterator	map<Key, T, Compare, Allocator>::lower_bound(const Key& key)	const	{
		node_pointer	tmp = this->_M_t.lower_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (const_iterator(tmp));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::iterator	map<Key, T, Compare, Allocator>::upper_bound(const Key& key)	{
		node_pointer	tmp = this->_M_t.upper_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (iterator(tmp));
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::const_iterator	map<Key, T, Compare, Allocator>::upper_bound(const Key& key)	const	{
		node_pointer	tmp = this->_M_t.upper_bound(key);

		if (tmp == NULL)
			return (this->end());
		else
			return (const_iterator(tmp));
	}

/***************************/
/* Observers for map class */
/***************************/
	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::key_compare	map<Key, T, Compare, Allocator>::key_comp()	const	{
		return (this->_M_t.comp);
	}

	template <class Key, class T, class Compare, class Allocator>
	typename map<Key, T, Compare, Allocator>::value_compare	map<Key, T, Compare, Allocator>::value_comp()	const	{
		return (value_compare(this->_M_t.comp));
	}

/************************************/
/* Comparison methods for map class */
/************************************/
	template <class Key, class T, class Compare, class Allocator>
	bool	operator== (const ft::map<Key, T, Compare, Allocator>& lhs,
						const ft::map<Key, T, Compare, Allocator>& rhs)	{
		return (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class Key, class T, class Compare, class Allocator>
	bool	operator!= (const ft::map<Key, T, Compare, Allocator>& lhs,
						const ft::map<Key, T, Compare, Allocator>& rhs)	{
		return (!(lhs.size() == rhs.size()) || !ft::equal(lhs.begin(), lhs.end(), rhs.begin()));
	}

	template <class Key, class T, class Compare, class Allocator>
	bool	operator< (const ft::map<Key, T, Compare, Allocator>& lhs,
						const ft::map<Key, T, Compare, Allocator>& rhs)	{
		return (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()));
	}

	template <class Key, class T, class Compare, class Allocator>
	bool	operator<= (const ft::map<Key, T, Compare, Allocator>& lhs,
						const ft::map<Key, T, Compare, Allocator>& rhs)	{
		if (ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()))
			return (true);
		else if (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()))
			return (true);
		else
			return (false);
	}

	template <class Key, class T, class Compare, class Allocator>
	bool	operator> (const ft::map<Key, T, Compare, Allocator>& lhs,
						const ft::map<Key, T, Compare, Allocator>& rhs)	{
		if (!ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		  && (lhs.size() > rhs.size() || !ft::equal(lhs.begin(), lhs.end(), rhs.begin())))
			return (true);
		else
			return (false);
	}

	template <class Key, class T, class Compare, class Allocator>
	bool	operator>= (const ft::map<Key, T, Compare, Allocator>& lhs,
						const ft::map<Key, T, Compare, Allocator>& rhs)	{
		if (!ft::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end())
		  && (lhs.size() > rhs.size() || !ft::equal(lhs.begin(), lhs.end(), rhs.begin())))
			return (true);
		else if (lhs.size() == rhs.size() && ft::equal(lhs.begin(), lhs.end(), rhs.begin()))
			return (true);
		else
			return (false);
	}

	template <class Key, class T, class Compare, class Allocator>
	void	swap(ft::map<Key, T, Compare, Allocator>& lhs,
					ft::map<Key, T, Compare, Allocator>& rhs)	{
		lhs.swap(rhs);
	}
}
