#include "../tester/container.hpp"
#include "map.hpp"
#include <map>
#include <iostream>
#include <fstream>
#include <sanitizer/asan_interface.h>

#if defined(__has_feature)
#  if __has_feature(address_sanitizer)
#	else
#	define __sanitizer_set_report_path($) 0
#  endif
#endif

#ifdef FT
# define NAMESPACE_STR "ft"
#else
# define NAMESPACE_STR "std"
#endif

#define CONTAINER_STR "map"
#define LOG_PATH "logs/"
#define EXTENSION ".txt"
#define ERR_LOG "err_logs/"

template <typename Key, typename Mapped>
int	launch_test()
{
	std::string	path = ft::redirect_err_path("error", typename NAMESPACE::map<Key, Mapped>::pointer());
	__sanitizer_set_report_path(path.c_str());
	
	access_method<NAMESPACE::map<Key, Mapped>, ft::map_flag > test1;
	construct_method<NAMESPACE::map<Key, Mapped>, ft::map_flag > test2;
	iterator_method<NAMESPACE::map<Key, Mapped>, ft::map_flag> test3;
	capacity_method<NAMESPACE::map<Key, Mapped>, ft::map_flag> test4;
	modifier_method<NAMESPACE::map<Key, Mapped>, ft::map_flag> test5;
	comparison_method<NAMESPACE::map<Key, Mapped>, ft::map_flag> test6;
	lookup_method<NAMESPACE::map<Key, Mapped>, ft::map_flag> test7;
	observers_method<NAMESPACE::map<Key, Mapped>, ft::map_flag> test8;
	test1.all();
	test2.all();
	test3.all();
	test4.all();
	test5.all();
	test6.all();
	test7.all();
	test8.all();
	return (0);
}

int	main()
{
	std::srand(time(NULL));
	launch_test<int, int>();
	launch_test<int, std::string>();
	launch_test<int, double>();
	return (0);
}
