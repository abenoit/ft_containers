#ifndef	MAP_HPP
# define	MAP_HPP

# include <limits>
# include "../includes/iterators.hpp"
# include "../includes/node.hpp"
# include "../includes/rb_tree.hpp"
# include "../includes/pair.hpp"

namespace ft	{
	template <class Key,
				class T,
				class Compare = std::less<Key>,
				class Allocator = std::allocator<ft::pair<const Key, T> > >
	class map	{
		public:
			typedef	Key										key_type;
			typedef	T										mapped_type;
			typedef	ft::pair<const Key, T>					value_type;
			typedef	std::size_t								size_type;
			typedef	std::ptrdiff_t							difference_type;
			typedef	Compare									key_compare;
			typedef	Allocator								allocator_type;
			typedef	value_type&								reference;
			typedef	const value_type&						const_reference;
			typedef typename Allocator::pointer				pointer;
			typedef	typename Allocator::const_pointer		const_pointer;
			typedef	ft::bd_iterator<value_type, false>		iterator;
			typedef	ft::bd_iterator<value_type, true>		const_iterator;
			typedef	ft::reverse_iterator<iterator>			reverse_iterator;
			typedef	ft::reverse_iterator<const_iterator>	const_reverse_iterator;

			class	value_compare	:	public std::binary_function<value_type, value_type, bool>
			{
				/* Either friend or non protected constructor. Befriending this subclass corresponds to implementation */
				friend class map;
				protected:
					Compare	comp;
					value_compare(Compare c) : comp(c) {};

				public:
					bool	operator()(const value_type& lhs, const value_type & rhs)	const	{ return comp(lhs.first, rhs.first); };
			};

		private:
			typedef ft::node<value_type, Allocator>			node_type;
			typedef	node_type*								node_pointer;
			typedef	rb_tree<key_type, mapped_type, Compare, Allocator>		data_type;
			data_type		_M_t;
			key_compare		_comp;

		/* DEBUG PRINT METHOD */
			void		print() const { _M_t.print();};

		public:
		/******************************/
		/* Constructors for map class */
		/******************************/
			map();
			explicit	map(const Compare& comp, const Allocator& alloc = Allocator());

			template<class InputIt>
			map(InputIt first, InputIt last,
				const Compare& comp = Compare(),
				const Allocator& alloc = Allocator());

			map(const map& other);

			~map();

			map&	operator= (const map& other);

			allocator_type	get_allocator()	const;


		
		/***************************/
		/* Accessors for map class */
		/***************************/
			T&	at(const Key& key);
			const T&	at(const Key& key) const;

			T&	operator[](const Key& key);

		/***************************/
		/* Iterators for map class */
		/***************************/
			iterator	begin();
			const_iterator	begin()	const;
			iterator	end();
			const_iterator	end()	const;
			reverse_iterator	rbegin();
			const_reverse_iterator	rbegin()	const;
			reverse_iterator	rend();
			const_reverse_iterator	rend()	const;

		/**********************************/
		/* Capacity methods for map class */
		/**********************************/
			bool	empty()	const;
			size_type	size()	const;
			size_type	max_size()	const;

		/***********************************/
		/* Modifiers methods for map class */
		/***********************************/
			void	clear();

			ft::pair<iterator, bool>	insert(const value_type& value);
			iterator	insert(iterator hint, const value_type& value);

			template<class InputIt>
			void	insert(InputIt first, InputIt last);

			void	erase(iterator	pos);
			void erase( iterator first, iterator last );
			size_type	erase(const	key_type& key);

			void	swap(map& other);

		/********************************/
		/* Lookup methods for map class */
		/********************************/
			size_type	count(const Key& key)	const;
			iterator	find(const Key& key);
			const_iterator	find(const Key& key)	const;
			ft::pair<iterator, iterator>	equal_range(const Key& key);
			ft::pair<const_iterator, const_iterator>	equal_range(const Key& key)	const;
			iterator	lower_bound(const Key& key);
			const_iterator	lower_bound(const Key& key)	const;
			iterator	upper_bound(const Key& key);
			const_iterator	upper_bound(const Key& key)	const;

		/***************************/
		/* Observers for map class */
		/***************************/
			key_compare	key_comp()	const;
			value_compare	value_comp()	const;
	};

	/************************************/
	/* Comparison methods for map class */
	/************************************/
	template <class Key, class T, class Compare, class Alloc>
	bool	operator== (const ft::map<Key, T, Compare, Alloc>& lhs,
						const ft::map<Key, T, Compare, Alloc>& rhs);

	template <class Key, class T, class Compare, class Alloc>
	bool	operator!= (const ft::map<Key, T, Compare, Alloc>& lhs,
						const ft::map<Key, T, Compare, Alloc>& rhs);

	template <class Key, class T, class Compare, class Alloc>
	bool	operator< (const ft::map<Key, T, Compare, Alloc>& lhs,
						const ft::map<Key, T, Compare, Alloc>& rhs);

	template <class Key, class T, class Compare, class Alloc>
	bool	operator<= (const ft::map<Key, T, Compare, Alloc>& lhs,
						const ft::map<Key, T, Compare, Alloc>& rhs);

	template <class Key, class T, class Compare, class Alloc>
	bool	operator> (const ft::map<Key, T, Compare, Alloc>& lhs,
						const ft::map<Key, T, Compare, Alloc>& rhs);

	template <class Key, class T, class Compare, class Alloc>
	bool	operator>= (const ft::map<Key, T, Compare, Alloc>& lhs,
						const ft::map<Key, T, Compare, Alloc>& rhs);

	template <class Key, class T, class Compare, class Alloc>
	void	swap(const ft::map<Key, T, Compare, Alloc>& lhs,
					const ft::map<Key, T, Compare, Alloc>& rhs);
}

# include "map.cpp"
#endif
