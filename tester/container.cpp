#include "container.hpp"

template <class container, typename flag>
container_handler<container, flag>::container_handler()	:
	ctnr1(init_container(flag(), container())),
	ctnr2(init_container(flag(), container())),
	const_ctnr(init_container(flag(), container()))
{
};

template <class container, typename flag>
void	container_handler<container, flag>::print(const container&	ctnr)	const {
	for (typename container::const_iterator	it = ctnr.begin(); it != ctnr.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
template <typename T>
void	container_handler<container, flag>::print(typename NAMESPACE::stack<T>& stack)	const {
	if (!stack.empty())
		std::cout << stack.top() << std::endl;
}
