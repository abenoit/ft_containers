#include <dirent.h>
#include "../includes/pair.hpp"
#include <vector>
#include <cstring>
#include <algorithm>

#ifndef NAMESPACE
# define NAMESPACE  std
#endif

#ifdef FT
# define NAMESPACE_STR "ft"
#else
# define NAMESPACE_STR "std"
#endif

#ifdef MAP
# define CONTAINER_STR "map"
#endif
#ifdef VECTOR
# define CONTAINER_STR "vector"
#endif
#ifdef STACK
# define CONTAINER_STR "stack"
#endif
#ifdef SET
# define CONTAINER_STR "set"
#endif

#ifndef CONTAINER_STR
# define CONTAINER_STR "unsupported container"
#endif

#define LOG_PATH "logs/"
#define EXTENSION ".txt"
#define ERR_LOG "err_logs/"

namespace	ft	{
	template <typename T>
	std::string	type_str(T* t)	{
		std::string ret("custom");
		(void)t;
		return (ret);
	}

	template <typename T>
	std::string	type_str(const T* t)	{
		std::string ret("const_custom");
		(void)t;
		return (ret);
	}

	template <>
	std::string	type_str(char* c)	{
		std::string ret("char");
		(void)c;
		return (ret);
	}

	template <>
	std::string	type_str(const char* c)	{
		std::string ret("const_char");
		(void)c;
		return (ret);
	}

	template <>
	std::string	type_str(int* i)	{
		std::string ret("int");
		(void)i;
		return (ret);
	}

	template <>
	std::string	type_str(const int* i)	{
		std::string ret("const_int");
		(void)i;
		return (ret);
	}

	template <>
	std::string	type_str(double* d)	{
		std::string ret("double");
		(void)d;
		return (ret);
	}

	template <>
	std::string	type_str(const double* d)	{
		std::string ret("const_double");
		(void)d;
		return (ret);
	}

	template <>
	std::string	type_str(float* f)	{
		std::string ret("float");
		(void)f;
		return (ret);
	}

	template <>
	std::string	type_str(const float* f)	{
		std::string ret("const_float");
		(void)f;
		return (ret);
	}

	template <>
	std::string	type_str(std::string* s)	{
		std::string ret("string");
		(void)s;
		return (ret);
	}

	template <>
	std::string	type_str(const std::string* s)	{
		std::string ret("const_string");
		(void)s;
		return (ret);
	}

	template <>
	std::string	type_str(char* *str)	{
		std::string ret("cstring");
		(void)str;
		return (ret);
	}


	template <>
	std::string	type_str(const char* *str)	{
		std::string ret("const_cstring");
		(void)str;
		return (ret);
	}

	template <typename T, typename U>
	std::string	type_str(NAMESPACE::pair<T, U>* p)	{
//		T	t = T();
//		U	u = U();
		std::string ret("pair_p");

		(void)p;
	//	ret += type_str(&t);
		ret += type_str(&p->first);
		ret += "_p";
//		ret += type_str(&u);
		ret += type_str(&p->second);
		return (ret);
	}

	template <typename T>
	std::string	redirect_path(const char *str, T* p)
	{
		std::string	type_name = type_str(p);
		std::string path = CONTAINER_STR;

		path += "/";
		path += LOG_PATH;
		path += NAMESPACE_STR;
		path += "_";
		path += type_name.c_str();
		path += "_";
		path += str;
		path += EXTENSION;
		return (path);
	}

	template <typename T>
	std::string	redirect_err_path(const char *str, T p)
	{
		std::string path = CONTAINER_STR;
		path += "/";
		path += ERR_LOG;
		path += NAMESPACE_STR;
		path += "_";
		path += type_str(p);
		path += "_";
		path += str;
		path += EXTENSION;
		return (path);
	}

	struct	container_flag	{
	};

	struct	sequence_flag	:	container_flag	{
	};

	struct	associative_flag	:	container_flag	{
	};

	struct	adaptor_flag	:	container_flag	{
	};

	struct	map_flag	:	associative_flag	{
	};

	struct	set_flag	:	associative_flag	{
	};

	struct	vec_flag	:	sequence_flag	{
	};

	struct	stack_flag	:	adaptor_flag	{
	};


	template <class container>
	const typename container::key_type	extract_key(typename container::const_iterator it, ft::set_flag)	{
		return (*it);
	}

	template <class container>
	const typename container::key_type	extract_key(typename container::const_iterator it, ft::map_flag)	{
		return (it->first);
	}
}

template <typename T>
T	fill(T*	p)	{
	(void)p;
	return (T());
}

std::string	fill(const std::string *p)	{
	static	size_t	i = 0;
	const std::string  tab[] = {
"Lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing",
"elit", "Nullam", "nulla", "urna", "feugiat", "at", "massa", "non", "fermentum",
"varius", "dui", "In", "ac", "dolor", "eget", "purus", "dapibus", "feugiat",
"Aliquam", "erat", "volutpat", "Curabitur", "fringilla", "nunc", "eu", "iaculis",
"varius", "justo", "velit", "ultricies", "neque", "eget", "pharetra", "eros",
"lacus", "sit", "amet", "purus", "Etiam", "fringilla", "ornare", "ipsum", "a",
"suscipit", "mauris", "congue", "a", "Class", "aptent", "taciti", "sociosqu", "ad",
"litora", "torquent", "per", "conubia", "nostra", "per", "inceptos", "himenaeos",
"Integer", "aliquam", "convallis", "nibh", "id", "vulputate", "Nam", "egestas",
"odio", "eget", "nulla", "end"};
	const std::string	ret = tab[i];

	(void)p;
	if (ret == "end")
		i = 0;
	else
		i++;
	return (ret);
}

std::string	fill(std::string *p)	{
	static	size_t	i = 0;
	std::string  tab[] = {
"Lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing",
"elit", "Nullam", "nulla", "urna", "feugiat", "at", "massa", "non", "fermentum",
"varius", "dui", "In", "ac", "dolor", "eget", "purus", "dapibus", "feugiat",
"Aliquam", "erat", "volutpat", "Curabitur", "fringilla", "nunc", "eu", "iaculis",
"varius", "justo", "velit", "ultricies", "neque", "eget", "pharetra", "eros",
"lacus", "sit", "amet", "purus", "Etiam", "fringilla", "ornare", "ipsum", "a",
"suscipit", "mauris", "congue", "a", "Class", "aptent", "taciti", "sociosqu", "ad",
"litora", "torquent", "per", "conubia", "nostra", "per", "inceptos", "himenaeos",
"Integer", "aliquam", "convallis", "nibh", "id", "vulputate", "Nam", "egestas",
"odio", "eget", "nulla", "end"};
	std::string	ret = tab[i];

	(void)p;
	if (ret == "end")
		i = 0;
	else
		i++;
	return (ret);
}

int	fill(int *p)	{
	static	size_t	i = 0;
	int tab[] = {1, 2 ,3, 325, 452, 782, 82953, 2985, 2905,
	928735, 963482, 2146473647, 87134, 7928532, 134, 45, 8,
	1234, 657, 365, 8741, 684, 654, 457, 113, 746, 541, 8101,
	1568, 541, 8412, 541, 876, 3419, 1, 9, 1, 64, 54, 54, 1,4,
	654, 18, 135, 523, 756, 2369, 124, 653, 375, 73, 5, 26, 23,
	6546, 346, 7, 0, 436, -42};
	int	ret = tab[i];

	(void)p;
	if (ret == -42)
		i = 0;
	else
		i++;
	return (ret);
}

int	fill(const int *p)	{
	static	size_t	i = 0;
	const int tab[] = {1, 2 ,3, 325, 452, 782, 82953, 2985, 2905,
	928735, 963482, 2146473647, 87134, 7928532, 134, 45, 8,
	1234, 657, 365, 8741, 684, 654, 457, 113, 746, 541, 8101,
	1568, 541, 8412, 541, 876, 3419, 1, 9, 1, 64, 54, 54, 1,4,
	654, 18, 135, 523, 756, 2369, 124, 653, 375, 73, 5, 26, 23,
	6546, 346, 7, 0, 436, -42};
	const int	ret = tab[i];

	(void)p;
	if (ret == -42)
		i = 0;
	else
		i++;
	return (ret);
}

double	fill(double *p)	{
	static	size_t	i = 0;
	double tab[] = {3.46323, 15.3251, 23.13541, 0.262153, 26315.2521, 1451.234642, 1435134.3521, 135.632, 1.36634, 0.1, 325.236, 124.6547, 962.554, 98.0, 1237.370, 247.0024, 123.78, 74.93, 873.4, 129.34, 9.5, 45.3, 9999999.99998, 92986.2, 28.25, 544.5, 233.8, 0.8764529, 3.141829384, 13.46, 23.09, 57.987, 459.0000000001, 347.174, -42.0};
	double	ret = tab[i];

	(void)p;
	if (ret == -42.0)
		i = 0;
	else
		i++;
	return (ret);
}

template <typename T, typename U>
NAMESPACE::pair<const T, U>	fill(NAMESPACE::pair<const T, U> *p)	{
	T	a = T();
	U	b = U();

	(void)p;
	return (NAMESPACE::make_pair(fill(&a), fill(&b)));
}

template <typename T, typename U>
NAMESPACE::pair<T, U>	fill(NAMESPACE::pair<T, U> *p)	{
	T	a = T();
	U	b = U();

	(void)p;
	return (NAMESPACE::make_pair(fill(&a), fill(&b)));
}

template <class container>
void	fill_size(container* c, typename container::size_type& n, ft::container_flag* f)	{
	(void)f;
	(void)n;
	*c = container();
}

template <class container>
void	fill_size(container* c, typename container::size_type& n, ft::vec_flag* f)	{
	(void)f;
	c->assign(n, fill(typename container::pointer()));
}

template <class container>
void	fill_size(container* c, typename container::size_type& n, ft::map_flag* f)	{
	(void)f;
	for (typename container::size_type i = 0; i < n; i++)
	{
		c->insert(fill(typename container::pointer()));
	}
}

template <typename T>
class	g_vec	{
	public:
		typedef typename T::value_type	value_type;
		typedef typename std::vector<value_type>	data_type;
		data_type	content;

		g_vec()	{
			while (content.size() < 10)
				content.push_back(fill(typename data_type::pointer()));
		};
		~g_vec(){};

	typename data_type::iterator fill_begin(T)	{
		return (content.begin());
	}

	typename data_type::iterator fill_end(T)	{
		return (content.end());
	}
};

template <typename T>
g_vec<T>&	c_vec()	{
	static g_vec<T>	t;
	return (t);
};

template	<typename T, typename U>
struct	iter	{
	typedef typename T::iterator	value;
};

template	<typename T>
struct	iter<T, ft::stack_flag>	{
	typedef	typename T::value_type	value;
};

template	<typename T, typename U>
struct	const_iter	{
	typedef typename T::const_iterator	value;
};

template	<typename T>
struct	const_iter<T, ft::stack_flag>	{
	typedef	typename T::value_type	value;
};

std::vector<std::string>	search_path(const char *name)
{
	DIR		*dirp;
	struct dirent	*buf;
	std::vector<std::string>	vec;

	dirp = opendir(name);
	if (dirp == NULL)
		return (vec);
	buf = readdir(dirp);
	while (buf != NULL)
	{
		if (strcmp(buf->d_name, ".") != 0 && strcmp(buf->d_name, "..") != 0)
			vec.push_back(buf->d_name);
		buf = readdir(dirp);
	}
	std::sort(vec.begin(), vec.end());
	closedir(dirp);
	return (vec);
}

template <typename T, typename U>
std::ostream& operator<<(std::ostream& o, const typename NAMESPACE::pair<T, U>& ref)	{
	o << "(" << ref.first << ", " << ref.second << ")";
	return (o);
}

template <class container>
container	init_container(ft::container_flag, container c)
{
	(void)c;
	return (container());
}

template <class container>
container	init_container(ft::associative_flag, container c)
{
	(void)c;
	container	ret(c_vec<container>().fill_begin(container()), c_vec<container>().fill_end(container()));
	return (ret);
}

template <class container>
container	init_container(ft::stack_flag, container c)
{
	(void)c;
	container	ret;
	typename container::value_type	tmp = typename container::value_type();
	for (int i = 0; i < 10; i++)
		ret.push(fill(&tmp));
	return (ret);
}

template <class container>
container	init_container(ft::vec_flag, container c)
{
	(void)c;
	container	ret(5, fill(typename container::pointer()));
	return (ret);
}
