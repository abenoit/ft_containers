#include "container.hpp"

template <class container, typename flag>
void	iterator_method<container, flag>::normal_it_construct(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal iterator construction test:" << std::endl;
	std::cout << "Default constructor:" << std::endl;
	typename container::iterator	it;
	std::cout << "Iterator successfully created." << std::endl;
	std::cout << "Copy assignation:" << std::endl;
	it = this->ctnr1.begin();
	std::cout << *it++ << std::endl;
	std::cout << "Copy construction:" << std::endl;
	typename container::iterator	cpy_it(it);
	std::cout << *cpy_it << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_it_construct(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const iterator construction test:" << std::endl;
	std::cout << "Default constructor:" << std::endl;
	typename container::const_iterator	it;
	std::cout << "Iterator successfully created." << std::endl;
	std::cout << "Copy assignation:" << std::endl;
	it = this->ctnr1.begin();
	std::cout << *it++ << std::endl;
	std::cout << "Copy construction:" << std::endl;
	typename container::const_iterator	cpy_it(it);
	std::cout << *cpy_it << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::normal_it_to_const_it(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal iterator to const iterator:" << std::endl;
	typename container::iterator	it(this->ctnr1.begin());
	typename container::const_iterator	cpy_it(it);
	std::cout << *cpy_it << std::endl;
}

/*
template <class container, typename flag>
void	iterator_method<container, flag>::const_it_to_normal_it(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const iterator to normal iterator:" << std::endl;
	typename container::const_iterator	it(this->const_ctnr.begin());
	typename container::iterator	cpy_it(it);
	std::cout << *cpy_it << std::endl;
}
*/

template <class container, typename flag>
void	iterator_method<container, flag>::normal_it_normal_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal iterator on normal container incrementation:" << std::endl;
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_it_normal_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const iterator on normal container incrementation:" << std::endl;
	for (typename container::const_iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_it_const_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const iterator on const container incrementation:" << std::endl;
	for (typename container::const_iterator it = this->const_ctnr.begin(); it != this->const_ctnr.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

/*
template <class container, typename flag>
void	iterator_method<container, flag>::normal_it_const_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal iterator on const container incrementation:" << std::endl;
	for (typename container::iterator it = this->const_ctnr.begin(); it != this->const_ctnr.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}
*/

template <class container, typename flag>
void	iterator_method<container, flag>::normal_it_normal_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal iterator on normal container decrementation:" << std::endl;
	for (typename container::iterator it = --this->ctnr1.end(); it != --this->ctnr1.begin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_it_normal_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const iterator on normal container decrementation:" << std::endl;
	for (typename container::const_iterator it = --this->ctnr1.end(); it != --this->ctnr1.begin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_it_const_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const iterator on const container decrementation:" << std::endl;
	for (typename container::const_iterator it = --this->const_ctnr.end(); it != --this->const_ctnr.begin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}

/*
template <class container, typename flag>
void	iterator_method<container, flag>::normal_it_const_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal iterator on const container decrementation:" << std::endl;
	for (typename container::iterator it = --this->const_ctnr.end(); it != --this->const_ctnr.begin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}
*/

template <class container, typename flag>
void	iterator_method<container, flag>::normal_rev_it_normal_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal reverse iterator on normal container incrementation:" << std::endl;
	for (typename container::reverse_iterator it = this->ctnr1.rbegin(); it != this->ctnr1.rend(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_rev_it_normal_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const reverse iterator on normal container incrementation:" << std::endl;
	for (typename container::const_reverse_iterator it = this->ctnr1.rbegin(); it != this->ctnr1.rend(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_rev_it_const_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const reverse iterator on const container incrementation:" << std::endl;
	for (typename container::const_reverse_iterator it = this->const_ctnr.rbegin(); it != this->const_ctnr.rend(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}

/*
template <class container, typename flag>
void	iterator_method<container, flag>::normal_rev_it_const_ct_inc(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal reverse iterator on const container incrementation:" << std::endl;
	for (typename container::reverse_iterator it = this->const_ctnr.rbegin(); it != this->const_ctnr.rend(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;
}
*/

template <class container, typename flag>
void	iterator_method<container, flag>::normal_rev_it_normal_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal reverse iterator on normal container decrementation:" << std::endl;
	for (typename container::reverse_iterator it = --this->ctnr1.rend(); it != this->ctnr1.rbegin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_rev_it_normal_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const reverse iterator on normal container decrementation:" << std::endl;
	for (typename container::const_reverse_iterator it = --this->ctnr1.rend(); it != this->ctnr1.rbegin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	iterator_method<container, flag>::const_rev_it_const_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Const reverse iterator on const container decrementation:" << std::endl;
	for (typename container::const_reverse_iterator it = --this->const_ctnr.rend(); it != this->const_ctnr.rbegin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}

/*
template <class container, typename flag>
void	iterator_method<container, flag>::normal_rev_it_const_ct_dec(ft::container_flag* p)	{
	(void)p;
	std::cout << "Normal reverse iterator on const container decrementation:" << std::endl;
	for (typename container::reverse_iterator it = --this->const_ctnr.rend(); it != this->const_ctnr.begin(); it--)
		std::cout << *it << " ";
	std::cout << std::endl;
}
*/

template <class container, typename flag>
void	iterator_method<container, flag>::all()	{
	typename container::value_type	v;
	flag	p;
	std::string	path = ft::redirect_path("iterator_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Iterator tests==" << std::endl;
	normal_it_construct(&p);
	const_it_construct(&p);
	normal_it_to_const_it(&p);
//	const_it_to_normal_it(&p);
	normal_it_normal_ct_inc(&p);
	const_it_normal_ct_inc(&p);
	const_it_const_ct_inc(&p);
//	normal_it_const_ct_inc(&p);
	normal_it_normal_ct_dec(&p);
	const_it_normal_ct_dec(&p);
	const_it_const_ct_dec(&p);
//	normal_it_const_ct_dec(&p);
	normal_rev_it_normal_ct_inc(&p);
	const_rev_it_normal_ct_inc(&p);
	const_rev_it_const_ct_inc(&p);
//	normal_rev_it_const_ct_inc(&p);
	normal_rev_it_normal_ct_dec(&p);
	const_rev_it_normal_ct_dec(&p);
	const_rev_it_const_ct_dec(&p);
//	normal_rev_it_const_ct_dec(&p);

	std::cout.rdbuf(coutbuf);
}
