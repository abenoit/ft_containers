
#include "container.hpp"

template <class container, typename flag>
void	modifier_method<container, flag>::print_before(container& ct, const char func[], ft::container_flag* p)	{
	(void)p;
	std::cout << "Container size before " << func << ": " << std::endl;
	std::cout << ct.size() << std::endl;
	std::cout << "Container content before " << func << ": " << std::endl;
	this->print(ct);
}

template <class container, typename flag>
void	modifier_method<container, flag>::print_before(container& ct, const char func[], ft::vec_flag* p)	{
	(void)p;
	std::cout << "Container size before " << func << ": " << std::endl;
	std::cout << ct.size() << std::endl;
	std::cout << "Container capacity before " << func << ": " << std::endl;
	std::cout << ct.capacity() << std::endl;
	std::cout << "Container content before " << func << ": " << std::endl;
	this->print(ct);
}

template <class container, typename flag>
void	modifier_method<container, flag>::print_after(container& ct, const char func[], ft::container_flag* p)	{
	(void)p;
	std::cout << "Container size after " << func << ": " << std::endl;
	std::cout << ct.size() << std::endl;
	std::cout << "Container content after " << func << ": " << std::endl;
	this->print(ct);
}

template <class container, typename flag>
void	modifier_method<container, flag>::print_after(container& ct, const char func[], ft::vec_flag* p)	{
	(void)p;
	std::cout << "Container size after " << func << ": " << std::endl;
	std::cout << ct.size() << std::endl;
	std::cout << "Container capacity after " << func << ": " << std::endl;
	std::cout << ct.capacity() << std::endl;
	std::cout << "Container content after " << func << ": " << std::endl;
	this->print(ct);
}

template <class container, typename flag>
void	modifier_method<container, flag>::clear_method(container& ct, ft::container_flag* p)	{
	(void)p;
	print_before(ct, "clear", p);
	try
	{
		ct.clear();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "clear", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::count_one_insert(container& ct, typename container::iterator it, ft::container_flag* p)	{
	(void)p;
	print_before(ct, "insert one", p);
	try
	{
		std::cout << "Insert return value:" << std::endl;
		std::cout << *ct.insert(it, fill(typename container::pointer())) << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "insert one", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::count_multiple_insert(container& ct, typename container::size_type n, typename container::iterator it, ft::vec_flag* p)	{
	(void)p;
	print_before(ct, "multiple insert", p);
	try
	{
		ct.insert(it, n, fill(typename container::pointer()));
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "multiple insert", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::range_insert(container& ct, typename container::iterator it, typename container::iterator it1, typename container::iterator it2, ft::vec_flag* p)	{
	(void)p;
	print_before(ct, "range insert", p);
	try
	{
		ct.insert(it, it1, it2);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "range insert", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::simple_erase(container& ct, typename container::iterator it, ft::container_flag* p)	{
	(void)p;
	print_before(ct, "simple erase", p);
	try
	{
		std::cout << "Attempting erase on iterator:" << std::endl;
		ct.erase(it);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "simple erase", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::simple_erase(container& ct, typename container::iterator it, ft::vec_flag* p)	{
	(void)p;
	print_before(ct, "simple erase", p);
	try
	{
		std::cout << "Erase return value:" << std::endl;
		std::cout << *ct.erase(it) << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "simple erase", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::range_erase(container& ct, typename container::iterator it1, typename container::iterator it2, ft::container_flag* p)	{
	(void)p;
	print_before(ct, "range erase", p);
	try
	{
		std::cout << "Attempting erase on range:" << std::endl;
		ct.erase(it1, it2);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "range erase", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::range_erase(container& ct, typename container::iterator it1, typename container::iterator it2, ft::vec_flag* p)	{
	(void)p;
	print_before(ct, "range erase", p);
	try
	{
		std::cout << "Erase return value:" << std::endl;
		std::cout << *ct.erase(it1, it2) << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	print_after(ct, "range erase", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::push(ft::vec_flag* p)	{
	(void)p;
	print_before(this->ctnr1, "push back", p);
	this->ctnr1.push_back(fill(typename container::pointer()));
	print_after(this->ctnr1, "push back", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::pop(ft::vec_flag* p)	{
	(void)p;
	print_before(this->ctnr1, "pop back", p);
	this->ctnr1.pop_back();
	print_after(this->ctnr1, "pop back", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::resize(container& ct, typename container::size_type n, ft::vec_flag* p)	{
	(void)p;
	print_before(ct, "resize", p);
	ct.resize(n);
	print_after(ct, "resize", p);
}

template <class container, typename flag>
void	modifier_method<container, flag>::swap_test(container& c1, container& c2, ft::container_flag* p)	{
	(void)p;
	std::cout << "Swap specialisation test" << std::endl;
	std::cout << "Container1 content before swap:" << std::endl;
	this->print(c1);
	std::cout << "Container2 content before swap:" << std::endl;
	this->print(c2);
	NAMESPACE::swap(c1, c2);
	std::cout << "Container1 content after swap:" << std::endl;
	this->print(c1);
	std::cout << "Container2 content after swap:" << std::endl;
	this->print(c2);
}

template <class container, typename flag>
void	modifier_method<container, flag>::all()	{
	typename container::value_type	v;
	flag	p;
	std::string	path = ft::redirect_path("modifier_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Modifier method tests==" << std::endl;
	std::cout << "Clear method" << std::endl;
	clear_method(this->ctnr1, &p);
	std::cout << "Simple inserts" << std::endl;
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	count_one_insert(this->ctnr1, this->ctnr1.begin(), &p);
	std::cout << "Count multiple insert" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	this->cpy_it1++;
	count_multiple_insert(this->ctnr1, 4, this->cpy_it1, &p);
	std::cout << "Range multiple insert" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	this->cpy_it2--;
	range_insert(this->ctnr2, this->ctnr2.begin()++, this->cpy_it1, this->cpy_it2, &p);
	std::cout << "Simple erase at random" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	this->cpy_it2--;
	simple_erase(this->ctnr1, this->cpy_it2, &p);
	std::cout << "Range erase at begin()" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	this->cpy_it1++;
	range_erase(this->ctnr1, this->ctnr1.begin(), this->cpy_it1, &p);
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	range_erase(this->ctnr1, this->cpy_it1, this->cpy_it2, &p);
	push(&p);
	pop(&p);
	resize(this->ctnr1, 2, &p);
	resize(this->ctnr1, 10, &p);
	swap_test(this->ctnr1, this->ctnr2, &p);

	std::cout.rdbuf(coutbuf);
}

template <class container>
void	modifier_method<container, ft::stack_flag>::print_before(container& ct, const char func[])	{
	std::cout << "Container size before " << func << ": " << std::endl;
	std::cout << ct.size() << std::endl;
	std::cout << "Container content before " << func << ": " << std::endl;
	this->print(ct);
}

template <class container>
void	modifier_method<container, ft::stack_flag>::print_after(container& ct, const char func[])	{
	std::cout << "Container size after " << func << ": " << std::endl;
	std::cout << ct.size() << std::endl;
	std::cout << "Container content after " << func << ": " << std::endl;
	this->print(ct);
}

template <class container>
void	modifier_method<container, ft::stack_flag>::push()	{
	typename container::value_type	tmp = typename container::value_type();
	print_before(this->ctnr1, "push");
	this->ctnr1.push(fill(&tmp));
	print_after(this->ctnr1, "push");
}

template <class container>
void	modifier_method<container, ft::stack_flag>::pop()	{
	print_before(this->ctnr1, "pop");
	this->ctnr1.pop();
	print_after(this->ctnr1, "pop");
}

template <class container>
void	modifier_method<container, ft::stack_flag>::all()	{
	typename container::value_type	v;
	std::string	path = ft::redirect_path("modifier_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Modifier method tests==" << std::endl;
	push();
	push();
	push();
	push();
	pop();

	std::cout.rdbuf(coutbuf);
}
