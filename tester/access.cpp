#ifdef FT
# define NAMESPACE_STR "ft"
#else
# define NAMESPACE_STR "std"
#endif

#include "container.hpp"

/********************************************/
/*       STACK SPECIALISED METHODS          */
/********************************************/
template <class container, typename flag>
void	access_method<container, flag>::top_normal_case(ft::stack_flag* p)	{
	(void)p;
	std::cout << "Top Normal Case:" << std::endl;
	std::cout << this->ctnr1.top() << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::top_empty_container(ft::stack_flag* p)	{
	(void)p;
	std::cout << "Top Empty Container:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			std::cout << this->empty_ctnr.top() << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.top()" << std::endl;
	}
}

/*
template <class container, typename flag>
void	access_method<container, flag>::const_top_normal_case(ft::stack_flag* p)	{
	(void)p;
	std::cout << "Const Top Normal Case:" << std::endl;
	std::cout << this->const_ctnr.top() << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_top_empty_container(ft::stack_flag* p)	{
	(void)p;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			std::cout << this->const_empty_ctnr.top() << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.top()" << std::endl;
	}
}
*/

/********************************************/
/*         MAP SPECIALISED METHODS          */
/********************************************/
template <class container, typename flag>
void	access_method<container, flag>::brackets_normal_case(ft::map_flag* p)	{
	(void)p;
	std::cout << "Operator[] Normal Case:" << std::endl;
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << this->ctnr1[it->first] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::brackets_overflow(ft::map_flag* p)	{
	(void)p;
	std::cout << "Operator[] Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = std::rand(), j = 0; j < 5; j++)
				this->ctnr1[i];
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.operator[]()" << std::endl;
	}
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << this->ctnr1[it->first] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::at_method_normal_case(ft::map_flag* p)	{
	(void)p;
	std::cout << "At Method Normal Case:" << std::endl;
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << this->ctnr1.at(it->first) << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::at_method_overflow(ft::map_flag* p)	{
	(void)p;
	std::cout << "At Method Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = std::rand(), j = 0; j < 25; j++)
				std::cout << this->ctnr1.at(i) << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.at()" << std::endl;
	}
}


template <class container, typename flag>
void	access_method<container, flag>::const_at_method_normal_case(ft::map_flag* p)	{
	(void)p;
	std::cout << "At Const Method Normal Case:" << std::endl;
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << this->const_ctnr.at(it->first) << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_at_method_overflow(ft::map_flag* p)	{
	(void)p;
	std::cout << "At Const Method Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = std::rand(), j = 0; j < 5; j++)
				std::cout << this->const_ctnr.at(i) << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.at()" << std::endl;
	}
}

/*
template <class container, typename flag>
void	access_method<container, flag>::const_brackets_normal_case(ft::map_flag* p)	{
	(void)p;
	std::cout << "Const Operator[] Normal Case:" << std::endl;
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << this->const_ctnr[it->first] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_brackets_overflow(ft::map_flag* p)	{
	(void)p;
	std::cout << "Const Operator[] Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = std::rand(), j = 0; j < 5; j++)
				this->const_ctnr[i];
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.operator[]" << std::endl;
	}
	for (typename container::iterator it = this->ctnr1.begin(); it != this->ctnr1.end(); it++)
		std::cout << this->const_ctnr[it->first] << " ";
	std::cout << std::endl;
}
*/

/********************************************/
/*         VEC SPECIALISED METHODS          */
/********************************************/
template <class container, typename flag>
void	access_method<container, flag>::brackets_normal_case(ft::vec_flag* p)	{
	(void)p;
	(void)p;
	std::cout << "Operator[] Normal Case:" << std::endl;
	for (uint32_t i = 0; i < this->ctnr1.size(); i++)
		std::cout << this->ctnr1[i] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::brackets_overflow(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Operator[] Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = 0; i < this->ctnr1.size() + 1; i++)
				std::cout << this->ctnr1[i] << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.operator[]()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::at_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "At Method Normal Case:" << std::endl;
	for (uint32_t i = 0; i < this->ctnr1.size(); i++)
		std::cout << this->ctnr1.at(i) << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::at_method_overflow(ft::vec_flag* p)	{
	(void)p;
	std::cout << "At Method Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = 0; i < this->ctnr1.size() + 1; i++)
				std::cout << this->ctnr1.at(i) << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.at()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::front_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Front Method Normal Case:" << std::endl;
	std::cout << this->ctnr1.front() << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::front_method_empty_container(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Front Method Empty Container:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			std::cout << this->empty_ctnr.front() << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.front()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::back_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Back Method Normal Case:" << std::endl;
	std::cout << this->ctnr1.back() << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::back_method_empty_container(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Back Method Empty Container:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			std::cout << this->empty_ctnr.back() << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.back()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::data_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Data Method Normal Case:" << std::endl;
	for (uint32_t i = 0; i < this->ctnr1.size(); i++)
		std::cout << this->ctnr1.data()[i] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::data_method_empty_container(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Data Method Empty Container:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = 0; i < this->empty_ctnr.size(); i++)
				std::cout << this->empty_ctnr.data()[i] << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.data()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::const_at_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "At Const Method Normal Case:" << std::endl;
	for (uint32_t i = 0; i < this->const_ctnr.size(); i++)
		std::cout << this->const_ctnr.at(i) << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_at_method_overflow(ft::vec_flag* p)	{
	(void)p;
	std::cout << "At Const Method Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = 0; i < this->const_ctnr.size() + 1; i++)
				std::cout << this->const_ctnr.at(i) << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.at()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::const_brackets_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Const Operator[] Normal Case:" << std::endl;
	for (uint32_t i = 0; i < this->const_ctnr.size(); i++)
		std::cout << this->const_ctnr[i] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_brackets_overflow(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Const Operator[] Overflow:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = 0; i < this->const_ctnr.size() + 1; i++)
				std::cout << this->const_ctnr[i] << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.operator[]" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::const_front_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Front Const Method Normal Case:" << std::endl;
	std::cout << this->const_ctnr.front() << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_front_method_empty_container(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Front Const Method Empty Container:" << std::endl;

	pid = fork();
	if (pid == 0)
	{
		try
		{
			std::cout << this->const_empty_ctnr.front() << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.front()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::const_back_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Back Const Method Normal Case:" << std::endl;
	std::cout << this->const_ctnr.back() << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_back_method_empty_container(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Back Const Method Empty Container:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			std::cout << this->const_empty_ctnr.back() << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.back()" << std::endl;
	}
}

template <class container, typename flag>
void	access_method<container, flag>::const_data_method_normal_case(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Data Const Method Normal Case:" << std::endl;
	for (uint32_t i = 0; i < this->ctnr1.size(); i++)
		std::cout << this->const_ctnr.data()[i] << " ";
	std::cout << std::endl;
}

template <class container, typename flag>
void	access_method<container, flag>::const_data_method_empty_container(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Data Const Method Empty Container:" << std::endl;
	pid = fork();
	if (pid == 0)
	{
		try
		{
			for (uint32_t i = 0; i < this->const_empty_ctnr.size(); i++)
				std::cout << this->const_empty_ctnr.data()[i] << " ";
			std::cout << std::endl;
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		exit(0);
	}
	else
	{
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret) || WIFSIGNALED(ret))
			std::cout << "Segfault on " << NAMESPACE_STR << "::container.data()" << std::endl;
	}
}

template <class container, typename flag>
void access_method<container, flag>::all()
{
	typename container::value_type	v;
	flag	p;
	std::string	path = ft::redirect_path("access_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Acces Methods==" << std::endl;
	top_normal_case(&p);
	top_empty_container(&p);
//	const_top_normal_case(&p);
//	const_top_empty_container(&p);
	brackets_normal_case(&p);
	brackets_overflow(&p);
	at_method_normal_case(&p);
	at_method_overflow(&p);
	front_method_normal_case(&p);
	front_method_empty_container(&p);
	back_method_normal_case(&p);
	back_method_empty_container(&p);
	data_method_normal_case(&p);
	data_method_empty_container(&p);
	const_brackets_normal_case(&p);
	const_brackets_overflow(&p);
	const_at_method_normal_case(&p);
	const_at_method_overflow(&p);
	const_front_method_normal_case(&p);
	const_front_method_empty_container(&p);
	const_back_method_normal_case(&p);
	const_back_method_empty_container(&p);
	const_data_method_normal_case(&p);
	const_data_method_empty_container(&p);

	std::cout.rdbuf(coutbuf);
}
