#include "container.hpp"

template <class container, typename flag>
void	construct_method<container, flag>::construct_method<container, flag>::default_construct(ft::container_flag* p)	{
	container	c1;

	(void)p;
	std::cout << "Default Construct:" << std::endl;
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::construct_exception(ft::container_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::construct_exception(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Construct Exception:" << std::endl;
	try
	{
		container	c1(this->ctnr1.max_size() + 1, typename container::value_type());
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}

template <class container, typename flag>
void	construct_method<container, flag>::copy_construct(ft::container_flag* p)	{
	container	c1(this->ctnr1);

	(void)p;
	std::cout << "Copy Constructor:" << std::endl;
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::alloc_construct(ft::stack_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::alloc_construct(ft::container_flag* p)	{
	typename container::size_type	n;
	container	c1(std::less<typename container::key_type>(), this->ctnr1.get_allocator());

	n = 10;
	std::cout << "Allocator Constructor:" << std::endl;
	fill_size(&c1, n, p);
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::alloc_construct(ft::vec_flag* p)	{
	typename container::size_type	n;
	container	c1(this->ctnr1.get_allocator());

	n = 10;
	std::cout << "Allocator Constructor:" << std::endl;
	fill_size(&c1, n, p);
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::count_construct_default(ft::container_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::count_construct_default(ft::vec_flag* p)	{
	container	c1(10);

	(void)p;
	std::cout << "Count Constructor default:" << std::endl;
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::count_construct_value(ft::container_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::count_construct_value(ft::vec_flag* p)	{
	container	c1(10, fill(typename container::pointer()));

	(void)p;
	std::cout << "Count Constructor with value:" << std::endl;
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::range_construct(ft::container_flag* p)	{
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	container	c1(this->cpy_it1, this->cpy_it2);

	(void)p;
	std::cout << "Range Constructor:" << std::endl;
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::range_construct(ft::stack_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::equal_operator(ft::container_flag* p)	{
	container	c1;

	(void)p;
	c1 = this->ctnr1;
	std::cout << "Equal Operator:" << std::endl;
	this->print(c1);
}

template <class container, typename flag>
void	construct_method<container, flag>::assign(ft::vec_flag* p)	{
	(void)p;
	this->ctnr1.assign(10, fill(typename container::pointer()));
	std::cout << "Assign:" << std::endl;
	this->print(this->ctnr1);
}

template <class container, typename flag>
void	construct_method<container, flag>::assign(ft::container_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::iterator_assign(ft::vec_flag* p)	{
	(void)p;
	this->ctnr1.assign(5, fill(typename container::pointer()));
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it2 = this->ctnr1.end();
	this->ctnr2.assign(this->cpy_it1, this->cpy_it2);
	std::cout << "Iterator Assign:" << std::endl;
	this->print(this->ctnr2);
}

template <class container, typename flag>
void	construct_method<container, flag>::iterator_assign(ft::container_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::assign_exception(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Assign Exception:" << std::endl;
	try
	{
		this->ctnr1.assign(this->ctnr1.max_size() + 1, fill(typename container::pointer()));
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}

template <class container, typename flag>
void	construct_method<container, flag>::assign_exception(ft::container_flag* p)	{
	(void)p;
}

template <class container, typename flag>
void	construct_method<container, flag>::all()	{
	typename container::value_type	v;
	flag	p;
	std::string	path = ft::redirect_path("construct_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Construction and Assignation Methods==" << std::endl;
	default_construct(&p);
	construct_exception(&p);
	copy_construct(&p);
	alloc_construct(&p);
	count_construct_default(&p);
	count_construct_value(&p);
	range_construct(&p);
	equal_operator(&p);
	assign(&p);
	iterator_assign(&p);
	assign_exception(&p);

	std::cout.rdbuf(coutbuf);
}
