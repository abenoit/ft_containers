#include "container.hpp"

template <class container, typename flag>
void	lookup_method<container, flag>::count_method()	{
	std::cout << "Count method:" << std::endl;
	std::cout << this->ctnr1.count(ft::extract_key<container>(this->ctnr1.begin(), flag())) << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::find_method()	{
	std::cout << "Find method:" << std::endl;
	std::cout << *this->ctnr1.find(ft::extract_key<container>(this->ctnr1.begin(), flag())) << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::const_find_method()	{
	std::cout << "Const Find method:" << std::endl;
	std::cout << *this->const_ctnr.find(ft::extract_key<container>(this->const_ctnr.begin(), flag())) << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::equal_range_method()	{
	std::cout << "Equal Range method:" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	std::cout << *this->ctnr1.equal_range(ft::extract_key<container>(this->cpy_it1, flag())).first << " "
	<< *this->ctnr1.equal_range(ft::extract_key<container>(this->cpy_it1, flag())).second << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::const_equal_range_method()	{
	std::cout << "Const Equal Range method:" << std::endl;
	this->const_it = this->const_ctnr.begin();
	this->const_it++;
	this->const_it++;
	this->const_it++;
	this->const_it++;
	this->const_it++;
	std::cout << *this->const_ctnr.equal_range(ft::extract_key<container>(this->const_it, flag())).first << " "
	<< *this->const_ctnr.equal_range(ft::extract_key<container>(this->const_it, flag())).second << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::lower_bound_method()	{
	std::cout << "Lower Bound method:" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	std::cout << *this->ctnr1.lower_bound(ft::extract_key<container>(this->cpy_it1, flag())) << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::const_lower_bound_method()	{
	std::cout << "Const Lower Bound method:" << std::endl;
	this->const_it = this->const_ctnr.begin();
	this->const_it++;
	this->const_it++;
	this->const_it++;
	this->const_it++;
	this->const_it++;
	std::cout << *this->const_ctnr.lower_bound(ft::extract_key<container>(this->const_it, flag())) << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::upper_bound_method()	{
	std::cout << "Upper Bound method:" << std::endl;
	this->cpy_it1 = this->ctnr1.begin();
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	this->cpy_it1++;
	std::cout << *this->ctnr1.upper_bound(ft::extract_key<container>(this->cpy_it1, flag())) << std::endl;
}

template <class container, typename flag>
void	lookup_method<container, flag>::const_upper_bound_method()	{
	std::cout << "Const Upper Bound method:" << std::endl;
	this->const_it = this->const_ctnr.begin();
	this->const_it++;
	this->const_it++;
	this->const_it++;
	this->const_it++;
	this->const_it++;
	std::cout << *this->const_ctnr.upper_bound(ft::extract_key<container>(this->const_it, flag())) << std::endl;
}


template <class container, typename flag>
void	lookup_method<container, flag>::all()	{
	typename container::value_type	v;

	std::string	path = ft::redirect_path("lookup_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Lookup methods==" << std::endl;
	count_method();
	find_method();
	const_find_method();
	equal_range_method();
	const_equal_range_method();
	lower_bound_method();
	const_lower_bound_method();
	upper_bound_method();
	const_upper_bound_method();

	std::cout.rdbuf(coutbuf);
}
