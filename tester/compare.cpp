#include "container.hpp"

template <class container, typename flag>
void	comparison_method<container, flag>::equal_operator(container& c1, container& c2)	{
	std::cout << "c1 == c2 : " << (c1 == c2) << std::endl;
}
template <class container, typename flag>
void	comparison_method<container, flag>::inequal_operator(container& c1, container& c2)	{
	std::cout << "c1 != c2 : " << (c1 != c2) << std::endl;
}

template <class container, typename flag>
void	comparison_method<container, flag>::inf_operator(container& c1, container& c2)	{
	std::cout << "c1 < c2 : " << (c1 < c2) << std::endl;
}

template <class container, typename flag>
void	comparison_method<container, flag>::infeq_operator(container& c1, container& c2)	{
	std::cout << "c1 <= c2 : " << (c1 <= c2) << std::endl;
}

template <class container, typename flag>
void	comparison_method<container, flag>::sup_operator(container& c1, container& c2)	{
	std::cout << "c1 > c2 : " << (c1 > c2) << std::endl;
}

template <class container, typename flag>
void	comparison_method<container, flag>::supeq_operator(container& c1, container& c2)	{
	std::cout << "c1 >= c2 : " << (c1 >= c2) << std::endl;
}

template <class container, typename flag>
void	comparison_method<container, flag>::comparison_method::all()	{
	typename container::value_type	v;
	std::string	path = ft::redirect_path("compare_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Comparison Operators==" << std::endl;
	equal_operator(this->ctnr1, this->ctnr2); 
	inequal_operator(this->ctnr1, this->ctnr2);
	inf_operator(this->ctnr1, this->ctnr2);
	infeq_operator(this->ctnr1, this->ctnr2);
	sup_operator(this->ctnr1, this->ctnr2);
	supeq_operator(this->ctnr1, this->ctnr2);

	std::cout.rdbuf(coutbuf);
}
