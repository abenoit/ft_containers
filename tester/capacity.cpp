#include "container.hpp"

template <class container, typename flag>
void	capacity_method<container, flag>::empty_method(ft::container_flag* p)	{
	(void)p;
	std::cout << "Empty method Normal container:" << std::endl;
	std::cout << this->ctnr1.empty() << std::endl;
	std::cout << "Empty method Empty container:" << std::endl;
	std::cout << this->empty_ctnr.empty() << std::endl;
}

template <class container, typename flag>
void	capacity_method<container, flag>::size_method(ft::container_flag* p)	{
	(void)p;
	std::cout << "Size method container 1:" << std::endl;
	std::cout << this->ctnr1.size() << std::endl;
	std::cout << "Size method container 2:" << std::endl;
	std::cout << this->ctnr2.size() << std::endl;
	std::cout << "Size method Empty container:" << std::endl;
	std::cout << this->empty_ctnr.empty() << std::endl;
}

template <class container, typename flag>
void	capacity_method<container, flag>::max_size_method(ft::container_flag* p)	{
	(void)p;
	std::cout << "Container max size:" << std::endl;
	std::cout << this->ctnr1.max_size() << std::endl;
}

template <class container, typename flag>
void	capacity_method<container, flag>::reserve_method(ft::vec_flag* p)	{
	(void)p;
	container	c1;
	std::cout << "Reserve Method:" << std::endl;
	std::cout << "Default constructed container capacity:" << std::endl;
	std::cout << c1.capacity() << std::endl;
	std::cout << "After reserve(2)" << std::endl;
	c1.reserve(2);
	std::cout << c1.capacity() << std::endl;
	std::cout << "After reserve(4)" << std::endl;
	c1.reserve(4);
	std::cout << c1.capacity() << std::endl;
	std::cout << "After reserve(25)" << std::endl;
	c1.reserve(25);
	std::cout << c1.capacity() << std::endl;
	std::cout << "After reserve(624)" << std::endl;
	c1.reserve(624);
	std::cout << c1.capacity() << std::endl;
	std::cout << "After reserve(736491)" << std::endl;
	c1.reserve(736491);
	std::cout << c1.capacity() << std::endl;
	std::cout << "After reserve(98623580)" << std::endl;
	c1.reserve(98623580);
	std::cout << c1.capacity() << std::endl;
}

template <class container, typename flag>
void	capacity_method<container, flag>::reserve_exception(ft::vec_flag* p)	{
	(void)p;
	container	c1;

	std::cout << "Reserve exception" << std::endl;
	std::cout << "Trying to reserve max_size()" << std::endl;
	pid = fork();
	if (pid == 0)
	{
	(void)p;
		try
		{
	(void)p;
			c1.reserve(c1.max_size());
		}
		catch (std::exception& e)
		{
	(void)p;
			std::cout << e.what() << std::endl;
		}
		std::cout << c1.capacity() << std::endl;
		exit(0);
	}
	else
	{
	(void)p;
		waitpid(pid, &ret, 0);
		if (WEXITSTATUS(ret))
			std::cout << "Crash on " << NAMESPACE_STR << "::container.reserve(max_size())" << std::endl;
	}
}

template <class container, typename flag>
void	capacity_method<container, flag>::capacity_method_test(ft::vec_flag* p)	{
	(void)p;
	std::cout << "Capacity method container 1:" << std::endl;
	std::cout << this->ctnr1.capacity() << std::endl;
	std::cout << "Capacity method container 2:" << std::endl;
	std::cout << this->ctnr2.capacity() << std::endl;
	std::cout << "Capacity method Empty container:" << std::endl;
	std::cout << this->empty_ctnr.capacity() << std::endl;
}

template <class container, typename flag>
void	capacity_method<container, flag>::all()	{
	typename container::value_type	v;
	flag	p;
	std::string	path = ft::redirect_path("capacity_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Capacity method tests==" << std::endl;
	empty_method(&p);
	size_method(&p);
	max_size_method(&p);
	reserve_method(&p);
	reserve_exception(&p);
	capacity_method_test(&p);

	std::cout.rdbuf(coutbuf);
}
