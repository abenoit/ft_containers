#ifndef CONTAINER_HPP
# define CONTAINER_HPP

#include <iostream>
#include "test_utils.hpp"
#include "../vector/vector.hpp"
#include <vector>
#include "../map/map.hpp"
#include <map>
#include "../set/set.hpp"
#include <set>
#include "../stack/stack.hpp"
#include <stack>
#include <iterator>
#include <unistd.h>
#include <sys/wait.h>
#include <fstream>
#include <cstring>
#include <stdint.h>

#ifdef MAP
#define CONTAINER_STR "map"
#endif
#ifdef VECTOR
#define CONTAINER_STR "vector"
#endif
#ifdef STACK
#define CONTAINER_STR "stack"
#endif
#ifdef SET
#define CONTAINER_STR "set"
#endif

#define LOG_PATH "logs/"
#define EXTENSION ".txt"
#define ERR_LOG "err_logs/"

template <class container, typename flag>
class	container_handler	{
	public:
		container	ctnr1;
		container	ctnr2;
		const container	const_ctnr;
		container	empty_ctnr;
		const container	const_empty_ctnr;

		typename iter<container, flag>::value	cpy_it1;
		typename iter<container, flag>::value	cpy_it2;

		typename const_iter<container, flag>::value	const_it;

		container_handler();
		/*
		template <typename T>
		container_handler(T);
		template <typename T>
		container_handler(NAMESPACE::vector<T>);
		template <typename T, typename U>
		container_handler(NAMESPACE::map<T, U>);
		template <typename T>
		container_handler(NAMESPACE::set<T>);
		template <typename T>
		container_handler(NAMESPACE::stack<T>);
		*/

		~container_handler(){};

		void	print(const container&) const;

		template <typename T>
		void	print(typename NAMESPACE::stack<T>&) const;
};

template <class container, typename flag>
class	access_method: public container_handler<container, flag>	{
	public:
		pid_t	pid;
		int		ret;

		void	all();

		void	top_normal_case(ft::container_flag*){};
		void	top_normal_case(ft::stack_flag*);

		void	top_empty_container(ft::container_flag*){};
		void	top_empty_container(ft::stack_flag*);

		void	const_top_normal_case(ft::container_flag*){};
		void	const_top_normal_case(ft::stack_flag*);

		void	const_top_empty_container(ft::container_flag*){};
		void	const_top_empty_container(ft::stack_flag*);

		void	brackets_normal_case(ft::container_flag*){};
		void	brackets_normal_case(ft::vec_flag*);
		void	brackets_normal_case(ft::map_flag*);

		void	brackets_overflow(ft::container_flag*){};
		void	brackets_overflow(ft::vec_flag*);
		void	brackets_overflow(ft::map_flag*);

		void	at_method_normal_case(ft::container_flag*){};
		void	at_method_normal_case(ft::vec_flag*);
		void	at_method_normal_case(ft::map_flag*);

		void	at_method_overflow(ft::container_flag*){};
		void	at_method_overflow(ft::vec_flag*);
		void	at_method_overflow(ft::map_flag*);

		void	front_method_normal_case(ft::container_flag*){};
		void	front_method_normal_case(ft::vec_flag*);

		void	front_method_empty_container(ft::container_flag*){};
		void	front_method_empty_container(ft::vec_flag*);

		void	back_method_normal_case(ft::container_flag*){};
		void	back_method_normal_case(ft::vec_flag*);

		void	back_method_empty_container(ft::container_flag*){};
		void	back_method_empty_container(ft::vec_flag*);

		void	data_method_normal_case(ft::container_flag*){};
		void	data_method_normal_case(ft::vec_flag*);

		void	data_method_empty_container(ft::container_flag*){};
		void	data_method_empty_container(ft::vec_flag*);

		void	const_brackets_normal_case(ft::container_flag*){};
		void	const_brackets_normal_case(ft::vec_flag*);
//		void	const_brackets_normal_case(ft::map_flag*);

		void	const_brackets_overflow(ft::container_flag*){};
		void	const_brackets_overflow(ft::vec_flag*);
//		void	const_brackets_overflow(ft::map_flag*);

		void	const_at_method_normal_case(ft::container_flag*){};
		void	const_at_method_normal_case(ft::vec_flag*);
		void	const_at_method_normal_case(ft::map_flag*);

		void	const_at_method_overflow(ft::container_flag*){};
		void	const_at_method_overflow(ft::vec_flag*);
		void	const_at_method_overflow(ft::map_flag*);

		void	const_front_method_normal_case(ft::container_flag*){};
		void	const_front_method_normal_case(ft::vec_flag*);

		void	const_front_method_empty_container(ft::container_flag*){};
		void	const_front_method_empty_container(ft::vec_flag*);

		void	const_back_method_normal_case(ft::container_flag*){};
		void	const_back_method_normal_case(ft::vec_flag*);

		void	const_back_method_empty_container(ft::container_flag*){};
		void	const_back_method_empty_container(ft::vec_flag*);

		void	const_data_method_normal_case(ft::container_flag*){};
		void	const_data_method_normal_case(ft::vec_flag*);

		void	const_data_method_empty_container(ft::container_flag*){};
		void	const_data_method_empty_container(ft::vec_flag*);

};


template <class container, typename flag>
class	construct_method	:	public container_handler<container, flag>	{

	public:
		void	all();
		void	default_construct(ft::container_flag* p);

		void	construct_exception(ft::container_flag* p);
		void	construct_exception(ft::vec_flag* p);

		void	copy_construct(ft::container_flag* p);

		void	alloc_construct(ft::container_flag* p);
		void	alloc_construct(ft::stack_flag* p);
		void	alloc_construct(ft::vec_flag* p);

		void	count_construct_default(ft::container_flag* p);
		void	count_construct_default(ft::vec_flag* p);

		void	count_construct_value(ft::container_flag* p);
		void	count_construct_value(ft::vec_flag* p);

		void	range_construct(ft::container_flag* p);
		void	range_construct(ft::stack_flag* p);

		void	equal_operator(ft::container_flag* p);

		void	assign(ft::container_flag*);
		void	assign(ft::vec_flag*);

		void	iterator_assign(ft::container_flag*);
		void	iterator_assign(ft::vec_flag*);

		void	assign_exception(ft::container_flag*);
		void	assign_exception(ft::vec_flag*);
};

template <class container, typename flag>
class	iterator_method	: public container_handler<container, flag>	{
	public:
		void	all();
		void	normal_it_construct(ft::container_flag*);
		void	const_it_construct(ft::container_flag*);
		void	normal_it_to_const_it(ft::container_flag*);
//		void	const_it_to_normal_it(ft::container_flag*);
		void	normal_it_normal_ct_inc(ft::container_flag*);
		void	const_it_normal_ct_inc(ft::container_flag*);
		void	const_it_const_ct_inc(ft::container_flag*);
//		void	normal_it_const_ct_inc(ft::container_flag*);
		void	normal_it_normal_ct_dec(ft::container_flag*);
		void	const_it_normal_ct_dec(ft::container_flag*);
		void	const_it_const_ct_dec(ft::container_flag*);
//		void	normal_it_const_ct_dec(ft::container_flag*);
		void	normal_rev_it_normal_ct_inc(ft::container_flag*);
		void	const_rev_it_normal_ct_inc(ft::container_flag*);
		void	const_rev_it_const_ct_inc(ft::container_flag*);
//		void	normal_rev_it_const_ct_inc(ft::container_flag*);
		void	normal_rev_it_normal_ct_dec(ft::container_flag*);
		void	const_rev_it_normal_ct_dec(ft::container_flag*);
		void	const_rev_it_const_ct_dec(ft::container_flag*);
//		void	normal_rev_it_const_ct_dec(ft::container_flag*);
};

template <class container>
class	iterator_method<container, ft::stack_flag>	: public container_handler<container, ft::stack_flag>	{
	public:
		void	all(){};
};

template <class container, typename flag>
class	capacity_method	:	public container_handler<container, flag>	{
	public:
		pid_t	pid;
		int		ret;

		void	all();
		void	empty_method(ft::container_flag*);
		void	size_method(ft::container_flag*);
		void	max_size_method(ft::container_flag*);
		void	max_size_method(ft::stack_flag*){};
		void	reserve_method(ft::container_flag*){};
		void	reserve_method(ft::vec_flag*);
		void	reserve_exception(ft::container_flag*){};
		void	reserve_exception(ft::vec_flag*);
		void	capacity_method_test(ft::container_flag*){};
		void	capacity_method_test(ft::vec_flag*);
};

template <class container, typename flag>
class	modifier_method	:	public container_handler<container, flag>	{
	public:
		pid_t	pid;
		int		ret;

		void	all();
		void	print_before(container&, const char func[], ft::container_flag*);
		void	print_before(container&, const char func[], ft::vec_flag*);
		void	print_after(container&, const char func[], ft::container_flag*);
		void	print_after(container&, const char func[], ft::vec_flag*);
		void	clear_method(container&, ft::container_flag*);
		void	count_one_insert(container&, typename container::iterator, ft::container_flag*);
		void	count_multiple_insert(container&, typename container::size_type, typename container::iterator, ft::container_flag*){};
		void	count_multiple_insert(container&, typename container::size_type, typename container::iterator, ft::vec_flag*);
		void	range_insert(container&, typename container::iterator, typename container::iterator, typename container::iterator, ft::container_flag*){};
		void	range_insert(container&, typename container::iterator, typename container::iterator, typename container::iterator, ft::vec_flag*);
		void	simple_erase(container&, typename container::iterator, ft::container_flag*);
		void	simple_erase(container&, typename container::iterator, ft::vec_flag*);
		void	range_erase(container&, typename container::iterator, typename container::iterator, ft::container_flag*);
		void	range_erase(container&, typename container::iterator, typename container::iterator, ft::vec_flag*);
		void	push(ft::container_flag*){};
		void	push(ft::vec_flag*);
		void	pop(ft::container_flag*){};
		void	pop(ft::vec_flag*);
		void	resize(container&, typename container::size_type, ft::container_flag*){};
		void	resize(container&, typename container::size_type, ft::vec_flag*);
		void	swap_test(container&, container&, ft::container_flag*);
};

template <class container>
class	modifier_method<container, ft::stack_flag>	:	public container_handler<container, ft::stack_flag>	{
	public:
		void	all();
		void	print_before(container&, const char func[]);
		void	print_after(container&, const char func[]);
		void	push();
		void	pop();
};

template <class container, typename flag>
class	comparison_method	:	public container_handler<container, flag>	{
	public:
		void	all();
		void	equal_operator(container&, container&);
		void	inequal_operator(container&, container&);
		void	inf_operator(container&, container&);
		void	infeq_operator(container&, container&);
		void	sup_operator(container&, container&);
		void	supeq_operator(container&, container&);
};

template <class container, typename flag>
class	lookup_method	:	public container_handler<container, flag>	{
	public:
		void	all();
		void	count_method();
		void	find_method();
		void	const_find_method();
		void	equal_range_method();
		void	const_equal_range_method();
		void	lower_bound_method();
		void	const_lower_bound_method();
		void	upper_bound_method();
		void	const_upper_bound_method();
};

template <class container, typename flag>
class	observers_method	:	public container_handler<container, flag>	{
	public:
		void	all();	
		void	key_comp_method();
		void	value_comp_method();
};

# include "container.cpp"
# include "compare.cpp"
# include "access.cpp"
# include "construct.cpp"
# include "iterator.cpp"
# include "capacity.cpp"
# include "modifier.cpp"
# include "lookup.cpp"
# include "observers.cpp"
#endif
