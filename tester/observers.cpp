#include "container.hpp"

template <class container, typename flag>
void	observers_method<container, flag>::key_comp_method()	{
	typename container::key_compare	fn = this->ctnr1.key_comp();
	typename container::key_type	tmp;
	typename container::key_type	n1 = fill(&tmp);
	typename container::key_type	n2 = fill(&tmp);

	std::cout << "Key compare method:" << std::endl;
	std::cout << "comp(" << n1 << ", " << n2 << "):" <<std::endl;
	std::cout << fn(n1, n2) << std::endl;
}

template <class container, typename flag>
void	observers_method<container, flag>::value_comp_method()	{
	typename container::value_compare	fn = this->ctnr1.value_comp();
	typename container::value_type	tmp;
	typename container::value_type	n1 = fill(&tmp);
	typename container::value_type	n2 = fill(&tmp);

	std::cout << "Value compare method:" << std::endl;
	std::cout << "comp(" << n1 << ", " << n2 << "):" <<std::endl;
	std::cout << fn(n1, n2) << std::endl;
}

template <class container, typename flag>
void	observers_method<container, flag>::all()	{
	typename container::value_type	v;

	std::string	path = ft::redirect_path("observer_method", &v);
	std::ofstream cout(path.c_str());
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(cout.rdbuf());

	std::cout << "==Obervers methods==" << std::endl;
	key_comp_method();
	value_comp_method();

	std::cout.rdbuf(coutbuf);
}
