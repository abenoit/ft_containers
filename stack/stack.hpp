#ifndef STACK_HPP
# define STACK_HPP

# include "../vector/vector.hpp"

namespace	ft	{
	template <class T, class Container = vector<T> >
	class	stack	{
		public:
			typedef Container							container_type;
			typedef typename Container::value_type		value_type;
			typedef typename Container::size_type		size_type;
			typedef typename Container::reference		reference;
			typedef typename Container::const_reference	const_reference;

		protected:
			container_type	c;

		public:
		/******************************************/
		/* Coplien's form methods for stack class */
		/******************************************/
			explicit stack(const Container& cont = Container());
			stack(const stack &other);
			virtual	~stack();

			stack & operator=(const stack& other);

		/**********************************/
		/* Access methods for stack class */
		/**********************************/
			reference	top();
			const_reference	top() const;

		/************************************/
		/* Capacity methods for stack class */
		/************************************/
			bool	empty()	const;
			size_type	size()	const;

		/*************************************/
		/* Modifiers methods for stack class */
		/*************************************/
			void	push(const value_type& value);
			void	pop();

		/**************************************/
		/* Comparison methods for stack class */
		/**************************************/
		template <class U, class Cont>
		friend bool operator== (const stack<U, Cont>& lhs,
							 const stack<U, Cont>& rhs);

		template <class U, class Cont>
		friend bool operator!= (const stack<U, Cont>& lhs,
							 const stack<U, Cont>& rhs);

		template <class U, class Cont>
		friend bool operator< (const stack<U, Cont>& lhs,
							const stack<U, Cont>& rhs);

		template <class U, class Cont>
		friend bool operator<= (const stack<U, Cont>& lhs,
							 const stack<U, Cont>& rhs);

		template <class U, class Cont>
		friend bool operator> (const stack<U, Cont>& lhs,
							const stack<U, Cont>& rhs);

		template <class U, class Cont>
		friend bool operator>= (const stack<U, Cont>& lhs,
							 const stack<U, Cont>& rhs);
	};
}

# include "stack.cpp"
#endif
