#include "stack.hpp"

namespace ft	{
/******************************************/
/* Coplien's form methods for stack class */
/******************************************/
	template <class T, class Container>
	stack<T, Container>::stack(const Container& cont)
	:	c(cont)	{};

	template <class T, class Container>
	stack<T, Container>::stack(const stack &other)
	:	c(other.c)	{};

	template <class T, class Container>
	stack<T, Container>::~stack()	{};

	template <class T, class Container>
	stack<T, Container>& stack<T, Container>::operator=(const stack& other)	{
		this->c = other.c;
		return (*this);
	}

/**********************************/
/* Access methods for stack class */
/**********************************/
	template <class T, class Container>
	typename stack<T, Container>::reference	stack<T, Container>::top()
	{
		return (c.back());
	}

	template <class T, class Container>
	typename stack<T, Container>::const_reference	stack<T, Container>::top() const
	{
		return (c.back());
	}

/************************************/
/* Capacity methods for stack class */
/************************************/
	template <class T, class Container>
	bool	stack<T, Container>::empty()	const
	{
		return (c.empty());
	}

	template <class T, class Container>
	typename stack<T, Container>::size_type	stack<T, Container>::size()	const
	{
		return (c.size());
	}

/**********************************/
/* Access methods for stack class */
/**********************************/
	template <class T, class Container>
	void	stack<T, Container>::push(const value_type& value)
	{
		c.push_back(value);
	}

	template <class T, class Container>
	void	stack<T, Container>::pop()
	{
		c.pop_back();
	}

/***************************************/
/* Comparison methods for stack class */
/***************************************/
	template <class T, class Container>
	bool operator== (const stack<T, Container>& lhs,
					 const stack<T, Container>& rhs)
	{
		return (lhs.c == rhs.c);
	}

	template <class T, class Container>
	bool operator!= (const stack<T, Container>& lhs,
					 const stack<T, Container>& rhs)
	{
		return (lhs.c != rhs.c);
	}

	template <class T, class Container>
	bool operator< (const stack<T, Container>& lhs,
					const stack<T, Container>& rhs)
	{
		return (lhs.c < rhs.c);
	}

	template <class T, class Container>
	bool operator<= (const stack<T, Container>& lhs,
					 const stack<T, Container>& rhs)
	{
		return (lhs.c <= rhs.c);
	}

	template <class T, class Container>
	bool operator> (const stack<T, Container>& lhs,
					const stack<T, Container>& rhs)
	{
		return (lhs.c > rhs.c);
	}

	template <class T, class Container>
	bool operator>= (const stack<T, Container>& lhs,
					 const stack<T, Container>& rhs)
	{
		return (lhs.c >= rhs.c);
	}
}
