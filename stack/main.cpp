#include "../tester/container.hpp"
#include "stack.hpp"
#include <stack>
#include <iostream>
#include <fstream>
#include <sanitizer/asan_interface.h>

#if defined(__has_feature)
#  if __has_feature(address_sanitizer)
#	else
#	define __sanitizer_set_report_path($) 0
#  endif
#endif

#ifdef FT
# define NAMESPACE_STR "ft"
#else
# define NAMESPACE_STR "std"
#endif

#define CONTAINER_STR "stack"
#define LOG_PATH "logs/"
#define EXTENSION ".txt"
#define ERR_LOG "err_logs/"

template <typename T>
int	launch_test()
{
	typename NAMESPACE::stack<T>::value_type	tmp;
	std::string	path = ft::redirect_err_path("error", &tmp);
	__sanitizer_set_report_path(path.c_str());
	
	access_method<NAMESPACE::stack<T>, ft::stack_flag > test1;
	construct_method<NAMESPACE::stack<T>, ft::stack_flag> test2;
	iterator_method<NAMESPACE::stack<T>, ft::stack_flag> test3;
	capacity_method<NAMESPACE::stack<T>, ft::stack_flag> test4;
	modifier_method<NAMESPACE::stack<T>, ft::stack_flag> test5;
	comparison_method<NAMESPACE::stack<T>, ft::stack_flag> test6;
	test1.all();
	test2.all();
	test3.all();
	test4.all();
	test5.all();
	test6.all();
	return (0);
}

int	main()
{
	std::srand(time(NULL));
	launch_test<int>();
	launch_test<std::string>();
	launch_test<double>();
	return (0);
}
