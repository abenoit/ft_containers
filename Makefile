CPP_SRC += main.cpp

CPP_OBJ = $(CPP_SRC:.cpp=.o)

CC = clang++

RM = rm -f

CPP_FLAGS += -Wall
CPP_FLAGS += -Werror
CPP_FLAGS += -Wextra
CPP_FLAGS += -std=c++98

NAME = container_test

ifeq ($(dependencies), 1)
CPP_FLAGS += -MMD
endif

ifeq ($(d), 0)
CPP_FLAGS += -g
CPP_FLAGS += -fsanitize=address
endif

ifeq ($(v), 0)
CPP_FLAGS += -v
endif

all:	$(NAME)

$(NAME):	$(CPP_OBJ)
	$(CC) $(CPP_FLAGS) -o $(NAME) $(CPP_OBJ)

%.o: %.cpp
	$(CC) $(CPP_FLAGS) -c $< -o $@

clean:
	$(RM) $(CPP_OBJ)

fclean:	clean
	$(RM) $(NAME)
	@$(RM) vector/logs/*
	@$(RM) vector/err_logs/*
	@$(RM) vector/diff/*
	@$(RM) stack/logs/*
	@$(RM) stack/err_logs/*
	@$(RM) stack/diff/*
	@$(RM) set/logs/*
	@$(RM) set/err_logs/*
	@$(RM) set/diff/*
	@$(RM) map/logs/*
	@$(RM) map/err_logs/*
	@$(RM) map/diff/*
	@make -sC vector fclean
	@make -sC map fclean
	@make -sC set fclean
	@make -sC stack fclean

re:	fclean all

.PHONY:	clean re fclean
